$(function() {
    
    
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    
    
    
	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};


	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

	//slider
	var handle = $( "#custom-handle-bookings" );
	    $( "#sliderBookings" ).slider({
	    	range: true,
	      	value: 500,
	      	min: 500,
	      	max: 16093,
	      	values: [ 1250, 12000 ],
	      create: function() {
	        // handle.text( $( this ).slider( "value" ) );
	      },
	      slide: function( event, ui ) {
	        var val = $('#sliderBookings').slider("option", "value");
	        $( "#sliderBookings" ).find('input').val(val);
	      }
	    });

	// calendar slider
	$('.calendars').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  dots: false,
	  fade: true,
	  touchMove: false,
	  swipe: false,
	  asNavFor: '.calendar-month'
	});
	$('.calendar-month').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.calendars',
	  dots: false,
	  centerMode: false,
	  focusOnSelect: true,
	  touchMove: false,
	  swipe: false,
	});


	$('.calendar__day').popover({
	    container: 'body',
	    html: true,
	    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
	})
	// hide on cancel
	$('body').on('click', '.popover .cancel', function(e) {
		e.preventDefault();
		$('.calendar__day').popover('hide');
	});
	// set hours add here
	$('body').on('click', '.popover .set', function(e) {
		e.preventDefault();
		console.log('set hours');
	});
	// set hours add here
	$('body').on('click', '.popover .contact', function(e) {
		e.preventDefault();
		console.log('contact');
	});

	// dropdown menu
	$('#dropdownMenu').parent().on('show.bs.dropdown', function (e) {
	  // do something…
	  // e.preventDefault();
	  console.log('test')
	})

	$('.modal .modal-body .btn.proceed').on('click', function(e){
		$('#bookModal').modal('hide');
	});

	$('.modal .modal-body .btn.continue').on('click', function(e){
		$('#proceedModal').modal('hide');
	});

});

// modal date pick
var availableDays = ["2019-1-2","2019-1-20","2019-1-4","2019-1-15"];
var disabledDays = ["2019-1-21","2019-1-24","2019-1-3","2019-1-28"];
var date = new Date();
jQuery(document).ready(function() { 
    $( "#datepicker").datepicker({ 
    	dayNamesMin: [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            for (i = 0; i < disabledDays.length; i++) {
                if($.inArray(y + '-' + (m+1) + '-' + d,disabledDays) != -1) {
                    //return [false];
                    return [true, 'ui-state-disabled', ''];
                }
            }
            for (i = 0; i < availableDays.length; i++) {
                if($.inArray(y + '-' + (m+1) + '-' + d,availableDays) != -1) {
                    //return [false];
                    return [true, 'ui-state-active', ''];
                }
            }
            return [true];
        },
        onSelect: function(date) {
            // console.log($(this));
      		$('#datepicker').popover({
      			trigger:"manual",
      			viewport:"#datepicker",
            	// container: '#datepicker',
            	// selector: '.ui-state-default.ui-state-active',
            	// placement: 'auto',
            	html: true,
            	title: 'Choose Time',
            	content: `
            	<div class="date">from 
    	        	<select class="one">
    				  <option value="0">12:00 am</option>
    				  <option value="1">1:00 am</option>
    				  <option value="2">2:00 am</option>
    				  <option value="3">3:00 am</option>
    				  <option value="4">4:00 am</option>
    				  <option value="5">5:00 am</option>
    				  <option value="6">6:00 am</option>
    				  <option value="7">7:00 am</option>
    				  <option value="8">8:00 am</option>
    				  <option value="9">9:00 am</option>
    				  <option value="10">10:00 am</option>
    				  <option value="11">11:00 am</option>
    				  <option value="12">12:00 pm</option>
    				  <option value="13">1:00 pm</option>
    				  <option value="14">2:00 pm</option>
    				  <option value="15">3:00 pm</option>
    				  <option value="16">4:00 pm</option>
    				  <option value="17">5:00 pm</option>
    				  <option value="18">6:00 pm</option>
    				  <option value="19">7:00 pm</option>
    				  <option value="20">8:00 pm</option>
    				  <option value="21">9:00 pm</option>
    				  <option value="22">10:00 pm</option>
    				  <option value="23">11:00 pm</option>
    				</select> 
    				to 
    				<select class="two">
    				  <option value="0">12:00 am</option>
    				  <option value="1">1:00 am</option>
    				  <option value="2">2:00 am</option>
    				  <option value="3">3:00 am</option>
    				  <option value="4">4:00 am</option>
    				  <option value="5">5:00 am</option>
    				  <option value="6">6:00 am</option>
    				  <option value="7">7:00 am</option>
    				  <option value="8">8:00 am</option>
    				  <option value="9">9:00 am</option>
    				  <option value="10">10:00 am</option>
    				  <option value="11">11:00 am</option>
    				  <option value="12">12:00 pm</option>
    				  <option value="13">1:00 pm</option>
    				  <option value="14">2:00 pm</option>
    				  <option value="15">3:00 pm</option>
    				  <option value="16">4:00 pm</option>
    				  <option value="17">5:00 pm</option>
    				  <option value="18">6:00 pm</option>
    				  <option value="19">7:00 pm</option>
    				  <option value="20">8:00 pm</option>
    				  <option value="21">9:00 pm</option>
    				  <option value="22">10:00 pm</option>
    				  <option value="23">11:00 pm</option>
    				</select>
    			</div>
            	<p class="small">Your choose 2 hours session - <b>70£</b></p><div class="buttons"><a href="#" class="continue">Continue</a></div>`
            })
            $('#datepicker').popover('show');
        },
    });


// Custom upload
    function readURL(input) {
      if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image-upload-wrap').hide();

          $('.file-upload-image').attr('src', e.target.result);
          $('.file-upload-content').show();

          $('.image-title').html(input.files[0].name);
        };

        reader.readAsDataURL(input.files[0]);

      } else {
        removeUpload();
      }
    }

    function removeUpload() {
      $('.file-upload-input').replaceWith($('.file-upload-input').clone());
      $('.file-upload-content').hide();
      $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });

// show tabs by click next
$('.register .modal-body #nav-step1 button.button').on('click', function(e){
    $('#nav-tab a[href="#nav-step2"]').tab('show')
});
$('.register .modal-body #nav-step2 button.button').on('click', function(e){
    $('#nav-tab a[href="#nav-step3"]').tab('show')
});
$('.register .modal-body #nav-step3 button.button').on('click', function(e){
    $('#nav-tab a[href="#nav-step4"]').tab('show')
});

    //Scripts only homepage
        var $slider = $('.slider-top');

        if ($slider.length) {
          var currentSlide;
          var slidesCount;
          var sliderCounter = document.createElement('div');
          sliderCounter.classList.add('slider__counter');
          
          var updateSliderCounter = function(slick, currentIndex) {
            currentSlide = slick.slickCurrentSlide() + 1;
            slidesCount = slick.slideCount;
            $(sliderCounter).text(currentSlide + ' of ' +slidesCount)
          };

          $slider.on('init', function(event, slick) {
            $('.slider-nav').append(sliderCounter);
            updateSliderCounter(slick);
          });

          $slider.on('afterChange', function(event, slick, currentSlide) {
            updateSliderCounter(slick, currentSlide);
          });

          $slider.slick({
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            appendArrows: $('.slider-nav'),
            responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 767,
                  settings: {
                    slidesToShow: 1,
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
        ]
        });
        }

        var $slider2 = $('.reviews');

        if ($slider2.length) {
          var currentSlide;
          var slidesCount;
          var sliderCounter2 = document.createElement('div');
          sliderCounter2.classList.add('slider__counter2');
          
          var updateSliderCounter2 = function(slick, currentIndex) {
            currentSlide = slick.slickCurrentSlide() + 1;
            slidesCount = slick.slideCount;
            $(sliderCounter2).text(currentSlide + ' of ' +slidesCount)
          };

          $slider2.on('init', function(event, slick) {
            $('.reviews-nav').append(sliderCounter2);
            updateSliderCounter2(slick);
          });

          $slider2.on('afterChange', function(event, slick, currentSlide) {
            updateSliderCounter2(slick, currentSlide);
          });

          $slider2.slick({
            dots: false,
            arrows: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            appendArrows: $('.reviews-nav'),
            responsive: [
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 1,
                  }
                },
                {
                  breakpoint: 767,
                  settings: {
                    slidesToShow: 1,
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
        ]
        });
        }

        //dropdown update title
        $('.dropdown .dropdown-item label').click(function(e){
            console.log(this.innerHTML)
            $(this).parents('.dropdown').find('.dropdown-toggle').text(this.innerHTML);
            $(this).parents('.dropdown').find('input[type="hidden"]').val(this.innerHTML);
        });

        // field masks
        //date
        var cleaveDate = new Cleave('.input-date', {
            date: true,
            datePattern: ['d', 'm', 'Y']
        });
        var cleaveDateCoach = new Cleave('.input-date-coach', {
            date: true,
            datePattern: ['d', 'm', 'Y']
        });
        var cleaveDateExpire = new Cleave('.input-date-expire', {
            date: true,
            datePattern: ['m', 'Y']
        });
        // phone
        var cleavePhone = new Cleave('.input-phone', {
            phone: true,
            phoneRegionCode: 'GB',
            prefix: '+44',
        });
        var cleavePhone = new Cleave('.input-phone-coach', {
            phone: true,
            phoneRegionCode: 'GB',
            prefix: '+44',
        });
        // credit card
        var cleaveCreditCard = new Cleave('.input-credit-card', {
            creditCard: true
        });
            

        // Next tab click
        $('#nav-step1 .register .button').on('click', function(e){
            e.preventDefault();
            $('#first_name_coach, #second_name_coach, #email_coach, #password_coach, #password_confirm_coach, #birthday_coach, #birthday_coach, #phone_number_coach').valid();
            if ($('#first_name_coach, #second_name_coach, #email_coach, #password_coach, #password_confirm_coach, #birthday_coach, #birthday_coach, #phone_number_coach').valid()) {
                $('#nav-tab a[href="#nav-step2"]').tab('show')
            }
        });

        $('#nav-step2 .register .button').on('click', function(e){
            e.preventDefault();

            // if($('#verified').val() == 1) {
                $('#nav-tab a[href="#nav-step3"]').tab('show')
            // }

        });

        $('#nav-step3 .register .button').on('click', function(e){
            e.preventDefault();

            $('#account_name, #account_number, #sort_code, #card_type, #card_number, #name_on_card, #billing_address, #expire_date, #cvc ').valid();
            if ($('#account_name, #account_number, #sort_code, #card_type, #card_number, #name_on_card, #billing_address, #expire_date, #cvc ').valid()) {
                $('#nav-tab a[href="#nav-step4"]').tab('show')
            }
        });

        $('#send_email').on('click', function(e){
            e.preventDefault();
            console.log('clicked')
            email_coach = $('#email_coach').val();

//http://sportme.teamalfy.com/admin/public/api/sendVerificationEmail

            $.post("https://sportme.teamalfy.com/admin/public/api/sendVerificationEmail", {
                'email' : email_coach,
                },
                function(response,status){ // Required Callback Function
                    $('#verified').val('1');
                    console.log(response);
                });
        });

        $('#send_code').on('click', function(e){
            e.preventDefault();
            
            phone_number = $('#phone_number_coach').val();
            var number = phone_number.replace(/[^A-Z0-9]/ig, "");

            $.post('http://sportme.teamalfy.com/admin/public/api/sendVerificationSMS', {
                'phone_number' : phone_number,
                },
                function(response,status){ // Required Callback Function
                    $('#verified').val('1');
                });
        });

        // register
        $('#registration').validate({
            submitHandler: function (form){
                console.log('clicked');
                
                first_name = $('#first_name').val();
                last_name = $('#last_name').val();
                email = $('#email').val();
                password = $('#password').val();
                password_confirm = $('#password_confirm').val();
                birthday = $('#birthday').val();
                phone = $('#phone').val();
                number = phone.replace(/[^A-Z0-9]/ig, "");
                sports_of_interest = $('#sports_of_interest').val();
                location = $('#location').val();
                $.post('http://sportme.teamalfy.com/admin/public/api/step_register', {
                    'user_type' : 1,
                    'first_name' : first_name,
                    'last_name' : last_name,
                    'email' : email,
                    'password' : password,
                    'birthday' : birthday,
                    'phone' : number,
                    'sports_of_interest' : sports_of_interest,
                    'location' : location
                    },
                    function(response,status){ // Required Callback Function
                        // alert("*----Received Data----*nnResponse : " + response+"nnStatus : " + status);
                        $("#registration")[0].reset();
                        window.location.href = "https://sportmedash.teamalfy.com/login";

                    });
             },
            ignore: "",
            rules: {
                first_name: {
                    required: true,
                    minlength: 2
                },
                last_name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                password_confirm: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                birthday: {
                    required: true,
                    minlength: 2
                },
                phone: {
                    required: true,
                },
                sports_of_interest: {
                    required: true,
                },
                location: {
                    required: true,
                },
            },
        });

        $('#coach_reg').on('click', function(e){
            e.preventDefault();
            console.log('clicked');
            alert("Hello FUCK");
            
            first_name_coach = $('#first_name_coach').val();
            second_name_coach = $('#second_name_coach').val();
            email_coach = $('#email_coach').val();
            password_coach = $('#password_coach').val();
            password_confirm_coach = $('#password_confirm_coach').val();
            birthday_coach = $('#birthday_coach').val();
            phone_number_coach = $('#phone_number_coach').val();
            number = phone_number_coach.replace(/[^A-Z0-9]/ig, "");
            code = $('#code').val();
            account_name = $('#account_name').val();
            account_number = $('#account_number').val();
            sort_code = $('#sort_code').val();
            card_type = $( "input[value='card_type']").val();
            card_number = $('#card_number').val();
            name_on_card = $('#name_on_card').val();
            billing_address = $('#billing_address').val();
            expire_date = $('#expire_date').val();
            cvc = $('#cvc').val();
            coach_in_list = $('#coach_in_list').val();
            photo = $('#photo').val();

            $.post('http://sportme.teamalfy.com/admin/public/api/step_register', {
                'user_type' : 2,
                'first_name_coach' : first_name_coach,
                'second_name_coach' : second_name_coach,
                'email_coach' : email_coach,
                'password_coach' : password_coach,
                'birthday_coach' : birthday_coach,
                'phone_number_coach' : number,
                'code' : code,
                'account_name' : account_name,
                'account_number' : account_number,
                'sort_code' : sort_code,
                'card_type' : card_type,
                'card_number' : card_number,
                'name_on_card' : name_on_card,
                'billing_address' : billing_address,
                'expire_date' : expire_date,
                'cvc' : cvc,
                'coach_in_list' : coach_in_list,
                'photo' : photo,
                },
                function(response,status){ // Required Callback Function
                    alert("*----Received Data----*nnResponse : " + response+"nnStatus : " + status);
                    $("#registration_coach")[0].reset();
                    window.location.href = "https://sportmedash.teamalfy.com/login";
                });
        });
        
        $('#registration_coach').validate({
            submitHandler: function (form){
                
             },
            ignore: "",
            rules: {
                first_name_coach: {
                    required: true,
                    minlength: 2
                },
                second_name_coach: {
                    required: true,
                    minlength: 2
                },
                email_coach: {
                    required: true,
                    email: true
                },
                password_coach: {
                    required: true,
                    minlength: 5
                },
                password_confirm_coach: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password_coach"
                },
                birthday_coach: {
                    required: true,
                    minlength: 2
                },
                phone_number_coach: {
                    required: true,
                },
                code: {
                    required: true,
                    minlength: 2
                },
                account_name: {
                    required: true,
                    minlength: 2
                },
                account_number: {
                    required: true,
                    minlength: 2
                },
                sort_code: {
                    required: true,
                    minlength: 2
                },
                card_type: {
                    required: true,
                    minlength: 2
                },
                card_number: {
                    required: true,
                    minlength: 2
                },
                name_on_card: {
                    required: true,
                    minlength: 2
                },
                billing_address: {
                    required: true,
                    minlength: 2
                },
                expire_date: {
                    required: true,
                    minlength: 2
                },
                cvc: {
                    required: true,
                    minlength: 3,
                    maxlength: 3
                },
                coach_in_list: {
                    required: true,
                    minlength: 2
                },
                photo: {
                    minlength: 2
                },
            },
        });

});