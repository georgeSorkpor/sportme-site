<?php

use App\Mail\SendMail;
use App\Mail\findCoachMail;
use App\Mail\imCoachEmailVerMail;
use App\Forgotpass_user;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Twilio\Rest\Client;
use Authy\AuthyApi;
use App\FindCoachUser;
use App\ImCoachUser;
use App\CoachBooking;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');
Route::get('about', 'MainController@about');
Route::get('contactus', 'MainController@contactus');
Route::get('login', 'MainController@login');
Route::get('careers', 'MainController@careers');


Route::get('findcoach/log', 'FindCoachUserController@login');
Route::get('findcoach/reg', 'FindCoachUserController@registation');
Route::post('findcoach/store', 'FindCoachUserController@store');
Route::get('findcoach/emv/{id}', 'FindCoachUserController@EmailVarif');
Route::get('findcoach/forgotpass', function(){
    try{
            return view('findcoach.forgotpass');
        }catch(\Exception $e){
            echo $e ;
        }
    
});
Route::get('findcoach/forgotpass/{id}', function($id){
    try{
    $id = base64_decode($id);
   $value = explode("_",$id);
   print_r($value);
   echo (date("h:i:s"). "<br>");
   $conditions = [
        ['email', '=', $value[1]],
        ['usertype', '=', $value[5]],
        ['destroyed_time_token', '>', date("Y-m-d h:m:s",strtotime("now"))],
        ['token', '=', $id],
    ];
   $value_db = Forgotpass_user::where($conditions)->first();
       if(count($value_db) >= 1){
           echo "Pass Change ";
           $user_id = $value[6];
       }
       else{
           
           return Redirect::to('findcoach/forgotpass')->withErrors(['Sorry , Try Again !!']);
       }
    }catch(Exception $e){
        return Redirect::to('findcoach/forgotpass')->withErrors(['Sorry , Try Again !!']);
    }
    
});
Route::post('findcoach/forgotpasstry', function(Request $request){
   //dd($request);
    $count = FindCoachUser::count();
    $count2 = Forgotpass_user::count() + 1;
    $matchThese = ['email' => $request->email,'usertype' => $request->user_type];
    $value = FindCoachUser::where($matchThese)->first();
    if(count($value) >= 1){
        if($value->isverifyemailstatus == 1 && $value->email_verified_at !== null && $value->remember_token == "0" ){
            $request->forgote_token = "Sportme_".$request->email."_".$request->forgote_token."_".$count."_".$count2."_".$request->user_type."_".$value->id ;
            
           $Userforgetpass = new Forgotpass_user;
           $Userforgetpass->usertype = $request->user_type;
           $Userforgetpass->email = $request->email;
           $Userforgetpass->token = $request->forgote_token;
           $Userforgetpass->destroyed_time_token = date("Y-m-d h:m:s",strtotime("+1 hours"));
          $Userforgetpass->save();
            
            Mail::send(new findCoachMail());
            return Redirect::to('findcoach/forgotpass')->withErrors(['Please Check Your Email. Otherwise this link will be expired ']);
        }else{
            return Redirect::to('/findcoach/forgotpass')->withErrors(['Please Verify Your Account']);
        }
    }
    else{
        return Redirect::to('findcoach/forgotpass')->withErrors(['Sorry , We Cannot Find This Email !!']);
    }
  
    
});
Route::get('findcoach/logout', function(){
    
    if(Cookie::has("FindCoachUserInfo_cookie")){
        Cookie::forget("FindCoachUserInfo_cookie");
        $cookie = Cookie::forget("FindCoachUserInfo_cookie");
        echo "FindCookie_".$cookie;
    }
   
    else{
         echo "Nothing Found";
    }
    session()->forget('FindCoachSuperUserInfo_session');
    $cookie = Cookie::forget("FindCoachUserInfo_cookie");
    return Redirect::to('/')->withCookie($cookie);
    
    
});

Route::post('findcoach/logInTry', function(Request $request){
    
    if($request->user_type == 1){
        
        
        $Supper_pass = env('SUPPER_USER_PASSWORD');
        
        if($request->password == $Supper_pass){
            $matchThese = ['email' => $request->email];
            $value = FindCoachUser::where($matchThese)->first();
               
                if(count($value)>=1){
                     
                    
                     $request->session()->put('FindCoachSuperUserInfo_session', "1_".$value->id);
                      //$request->session()->forget('FindCoachSuperUserInfo_session');
                     //echo $request->session()->get('FindCoachSuperUserInfo_session');
                     return Redirect::to('/profile/findcoach/home');
                     dd($value);
                }else{
                    return Redirect::to('/findcoach/log')->withErrors(['Sorry , We Cannot Find This User !!']);
                }
                //Cookie::queue("FindCoachSupperUserInfo__cookie", "1_".$value->id , (86400*30));
                //$id = Cookie::get('FindCoachUserInfo_cookie');
                //return Redirect::to('/profile/findcoach/home');
             
            
            
        }
        
        
        $pass= md5($request->password);
        $matchThese = ['email' => $request->email,'password' => $pass];
        
         $value = FindCoachUser::where($matchThese)->first();
         if(count($value) >= 1){
             if($value->isverifyemailstatus == 1 && $value->email_verified_at !== null && $value->remember_token == "0" ){
             
                Cookie::queue("FindCoachUserInfo_cookie", "1_".$value->id , (86400*30));
                $id = Cookie::get('FindCoachUserInfo_cookie');
                return Redirect::to('/profile/findcoach/home');
             }
             else{
                  return Redirect::to('/findcoach/log')->withErrors(['Please Verify Your Account']);
             }
           
            
             
         }
         else{
             return Redirect::to('/findcoach/log')->withErrors(['Sorry , We Cannot Find This User !!']);
         }
    }
    
    else if($request->user_type == 2){
        
    }
});









//Start login system part of i m coach
Route::get('imcoach/log', 'ImCoachUserController@loginFormGet');
Route::post('imcoach/logintry', 'ImCoachUserController@loginFormPost');


Route::get('imcoach/reg/{id}', 'ImCoachUserController@regFormGet');
Route::post('imcoach/regtry/{id}', 'ImCoachUserController@regFormPost');
Route::post('imcoach/mobileVarif',function(Request $request){
    try{
        
        
    $countryArray = array(
    	'AD'=>array('name'=>'ANDORRA','code'=>'376'),
    	'AE'=>array('name'=>'UNITED ARAB EMIRATES','code'=>'971'),
    	'AF'=>array('name'=>'AFGHANISTAN','code'=>'93'),
    	'AG'=>array('name'=>'ANTIGUA AND BARBUDA','code'=>'1268'),
    	'AI'=>array('name'=>'ANGUILLA','code'=>'1264'),
    	'AL'=>array('name'=>'ALBANIA','code'=>'355'),
    	'AM'=>array('name'=>'ARMENIA','code'=>'374'),
    	'AN'=>array('name'=>'NETHERLANDS ANTILLES','code'=>'599'),
    	'AO'=>array('name'=>'ANGOLA','code'=>'244'),
    	'AQ'=>array('name'=>'ANTARCTICA','code'=>'672'),
    	'AR'=>array('name'=>'ARGENTINA','code'=>'54'),
    	'AS'=>array('name'=>'AMERICAN SAMOA','code'=>'1684'),
    	'AT'=>array('name'=>'AUSTRIA','code'=>'43'),
    	'AU'=>array('name'=>'AUSTRALIA','code'=>'61'),
    	'AW'=>array('name'=>'ARUBA','code'=>'297'),
    	'AZ'=>array('name'=>'AZERBAIJAN','code'=>'994'),
    	'BA'=>array('name'=>'BOSNIA AND HERZEGOVINA','code'=>'387'),
    	'BB'=>array('name'=>'BARBADOS','code'=>'1246'),
    	'BD'=>array('name'=>'BANGLADESH','code'=>'880'),
    	'BE'=>array('name'=>'BELGIUM','code'=>'32'),
    	'BF'=>array('name'=>'BURKINA FASO','code'=>'226'),
    	'BG'=>array('name'=>'BULGARIA','code'=>'359'),
    	'BH'=>array('name'=>'BAHRAIN','code'=>'973'),
    	'BI'=>array('name'=>'BURUNDI','code'=>'257'),
    	'BJ'=>array('name'=>'BENIN','code'=>'229'),
    	'BL'=>array('name'=>'SAINT BARTHELEMY','code'=>'590'),
    	'BM'=>array('name'=>'BERMUDA','code'=>'1441'),
    	'BN'=>array('name'=>'BRUNEI DARUSSALAM','code'=>'673'),
    	'BO'=>array('name'=>'BOLIVIA','code'=>'591'),
    	'BR'=>array('name'=>'BRAZIL','code'=>'55'),
    	'BS'=>array('name'=>'BAHAMAS','code'=>'1242'),
    	'BT'=>array('name'=>'BHUTAN','code'=>'975'),
    	'BW'=>array('name'=>'BOTSWANA','code'=>'267'),
    	'BY'=>array('name'=>'BELARUS','code'=>'375'),
    	'BZ'=>array('name'=>'BELIZE','code'=>'501'),
    	'CA'=>array('name'=>'CANADA','code'=>'1'),
    	'CC'=>array('name'=>'COCOS (KEELING) ISLANDS','code'=>'61'),
    	'CD'=>array('name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE','code'=>'243'),
    	'CF'=>array('name'=>'CENTRAL AFRICAN REPUBLIC','code'=>'236'),
    	'CG'=>array('name'=>'CONGO','code'=>'242'),
    	'CH'=>array('name'=>'SWITZERLAND','code'=>'41'),
    	'CI'=>array('name'=>'COTE D IVOIRE','code'=>'225'),
    	'CK'=>array('name'=>'COOK ISLANDS','code'=>'682'),
    	'CL'=>array('name'=>'CHILE','code'=>'56'),
    	'CM'=>array('name'=>'CAMEROON','code'=>'237'),
    	'CN'=>array('name'=>'CHINA','code'=>'86'),
    	'CO'=>array('name'=>'COLOMBIA','code'=>'57'),
    	'CR'=>array('name'=>'COSTA RICA','code'=>'506'),
    	'CU'=>array('name'=>'CUBA','code'=>'53'),
    	'CV'=>array('name'=>'CAPE VERDE','code'=>'238'),
    	'CX'=>array('name'=>'CHRISTMAS ISLAND','code'=>'61'),
    	'CY'=>array('name'=>'CYPRUS','code'=>'357'),
    	'CZ'=>array('name'=>'CZECH REPUBLIC','code'=>'420'),
    	'DE'=>array('name'=>'GERMANY','code'=>'49'),
    	'DJ'=>array('name'=>'DJIBOUTI','code'=>'253'),
    	'DK'=>array('name'=>'DENMARK','code'=>'45'),
    	'DM'=>array('name'=>'DOMINICA','code'=>'1767'),
    	'DO'=>array('name'=>'DOMINICAN REPUBLIC','code'=>'1809'),
    	'DZ'=>array('name'=>'ALGERIA','code'=>'213'),
    	'EC'=>array('name'=>'ECUADOR','code'=>'593'),
    	'EE'=>array('name'=>'ESTONIA','code'=>'372'),
    	'EG'=>array('name'=>'EGYPT','code'=>'20'),
    	'ER'=>array('name'=>'ERITREA','code'=>'291'),
    	'ES'=>array('name'=>'SPAIN','code'=>'34'),
    	'ET'=>array('name'=>'ETHIOPIA','code'=>'251'),
    	'FI'=>array('name'=>'FINLAND','code'=>'358'),
    	'FJ'=>array('name'=>'FIJI','code'=>'679'),
    	'FK'=>array('name'=>'FALKLAND ISLANDS (MALVINAS)','code'=>'500'),
    	'FM'=>array('name'=>'MICRONESIA, FEDERATED STATES OF','code'=>'691'),
    	'FO'=>array('name'=>'FAROE ISLANDS','code'=>'298'),
    	'FR'=>array('name'=>'FRANCE','code'=>'33'),
    	'GA'=>array('name'=>'GABON','code'=>'241'),
    	'GB'=>array('name'=>'UNITED KINGDOM','code'=>'44'),
    	'GD'=>array('name'=>'GRENADA','code'=>'1473'),
    	'GE'=>array('name'=>'GEORGIA','code'=>'995'),
    	'GH'=>array('name'=>'GHANA','code'=>'233'),
    	'GI'=>array('name'=>'GIBRALTAR','code'=>'350'),
    	'GL'=>array('name'=>'GREENLAND','code'=>'299'),
    	'GM'=>array('name'=>'GAMBIA','code'=>'220'),
    	'GN'=>array('name'=>'GUINEA','code'=>'224'),
    	'GQ'=>array('name'=>'EQUATORIAL GUINEA','code'=>'240'),
    	'GR'=>array('name'=>'GREECE','code'=>'30'),
    	'GT'=>array('name'=>'GUATEMALA','code'=>'502'),
    	'GU'=>array('name'=>'GUAM','code'=>'1671'),
    	'GW'=>array('name'=>'GUINEA-BISSAU','code'=>'245'),
    	'GY'=>array('name'=>'GUYANA','code'=>'592'),
    	'HK'=>array('name'=>'HONG KONG','code'=>'852'),
    	'HN'=>array('name'=>'HONDURAS','code'=>'504'),
    	'HR'=>array('name'=>'CROATIA','code'=>'385'),
    	'HT'=>array('name'=>'HAITI','code'=>'509'),
    	'HU'=>array('name'=>'HUNGARY','code'=>'36'),
    	'ID'=>array('name'=>'INDONESIA','code'=>'62'),
    	'IE'=>array('name'=>'IRELAND','code'=>'353'),
    	'IL'=>array('name'=>'ISRAEL','code'=>'972'),
    	'IM'=>array('name'=>'ISLE OF MAN','code'=>'44'),
    	'IN'=>array('name'=>'INDIA','code'=>'91'),
    	'IQ'=>array('name'=>'IRAQ','code'=>'964'),
    	'IR'=>array('name'=>'IRAN, ISLAMIC REPUBLIC OF','code'=>'98'),
    	'IS'=>array('name'=>'ICELAND','code'=>'354'),
    	'IT'=>array('name'=>'ITALY','code'=>'39'),
    	'JM'=>array('name'=>'JAMAICA','code'=>'1876'),
    	'JO'=>array('name'=>'JORDAN','code'=>'962'),
    	'JP'=>array('name'=>'JAPAN','code'=>'81'),
    	'KE'=>array('name'=>'KENYA','code'=>'254'),
    	'KG'=>array('name'=>'KYRGYZSTAN','code'=>'996'),
    	'KH'=>array('name'=>'CAMBODIA','code'=>'855'),
    	'KI'=>array('name'=>'KIRIBATI','code'=>'686'),
    	'KM'=>array('name'=>'COMOROS','code'=>'269'),
    	'KN'=>array('name'=>'SAINT KITTS AND NEVIS','code'=>'1869'),
    	'KP'=>array('name'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF','code'=>'850'),
    	'KR'=>array('name'=>'KOREA REPUBLIC OF','code'=>'82'),
    	'KW'=>array('name'=>'KUWAIT','code'=>'965'),
    	'KY'=>array('name'=>'CAYMAN ISLANDS','code'=>'1345'),
    	'KZ'=>array('name'=>'KAZAKSTAN','code'=>'7'),
    	'LA'=>array('name'=>'LAO PEOPLES DEMOCRATIC REPUBLIC','code'=>'856'),
    	'LB'=>array('name'=>'LEBANON','code'=>'961'),
    	'LC'=>array('name'=>'SAINT LUCIA','code'=>'1758'),
    	'LI'=>array('name'=>'LIECHTENSTEIN','code'=>'423'),
    	'LK'=>array('name'=>'SRI LANKA','code'=>'94'),
    	'LR'=>array('name'=>'LIBERIA','code'=>'231'),
    	'LS'=>array('name'=>'LESOTHO','code'=>'266'),
    	'LT'=>array('name'=>'LITHUANIA','code'=>'370'),
    	'LU'=>array('name'=>'LUXEMBOURG','code'=>'352'),
    	'LV'=>array('name'=>'LATVIA','code'=>'371'),
    	'LY'=>array('name'=>'LIBYAN ARAB JAMAHIRIYA','code'=>'218'),
    	'MA'=>array('name'=>'MOROCCO','code'=>'212'),
    	'MC'=>array('name'=>'MONACO','code'=>'377'),
    	'MD'=>array('name'=>'MOLDOVA, REPUBLIC OF','code'=>'373'),
    	'ME'=>array('name'=>'MONTENEGRO','code'=>'382'),
    	'MF'=>array('name'=>'SAINT MARTIN','code'=>'1599'),
    	'MG'=>array('name'=>'MADAGASCAR','code'=>'261'),
    	'MH'=>array('name'=>'MARSHALL ISLANDS','code'=>'692'),
    	'MK'=>array('name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','code'=>'389'),
    	'ML'=>array('name'=>'MALI','code'=>'223'),
    	'MM'=>array('name'=>'MYANMAR','code'=>'95'),
    	'MN'=>array('name'=>'MONGOLIA','code'=>'976'),
    	'MO'=>array('name'=>'MACAU','code'=>'853'),
    	'MP'=>array('name'=>'NORTHERN MARIANA ISLANDS','code'=>'1670'),
    	'MR'=>array('name'=>'MAURITANIA','code'=>'222'),
    	'MS'=>array('name'=>'MONTSERRAT','code'=>'1664'),
    	'MT'=>array('name'=>'MALTA','code'=>'356'),
    	'MU'=>array('name'=>'MAURITIUS','code'=>'230'),
    	'MV'=>array('name'=>'MALDIVES','code'=>'960'),
    	'MW'=>array('name'=>'MALAWI','code'=>'265'),
    	'MX'=>array('name'=>'MEXICO','code'=>'52'),
    	'MY'=>array('name'=>'MALAYSIA','code'=>'60'),
    	'MZ'=>array('name'=>'MOZAMBIQUE','code'=>'258'),
    	'NA'=>array('name'=>'NAMIBIA','code'=>'264'),
    	'NC'=>array('name'=>'NEW CALEDONIA','code'=>'687'),
    	'NE'=>array('name'=>'NIGER','code'=>'227'),
    	'NG'=>array('name'=>'NIGERIA','code'=>'234'),
    	'NI'=>array('name'=>'NICARAGUA','code'=>'505'),
    	'NL'=>array('name'=>'NETHERLANDS','code'=>'31'),
    	'NO'=>array('name'=>'NORWAY','code'=>'47'),
    	'NP'=>array('name'=>'NEPAL','code'=>'977'),
    	'NR'=>array('name'=>'NAURU','code'=>'674'),
    	'NU'=>array('name'=>'NIUE','code'=>'683'),
    	'NZ'=>array('name'=>'NEW ZEALAND','code'=>'64'),
    	'OM'=>array('name'=>'OMAN','code'=>'968'),
    	'PA'=>array('name'=>'PANAMA','code'=>'507'),
    	'PE'=>array('name'=>'PERU','code'=>'51'),
    	'PF'=>array('name'=>'FRENCH POLYNESIA','code'=>'689'),
    	'PG'=>array('name'=>'PAPUA NEW GUINEA','code'=>'675'),
    	'PH'=>array('name'=>'PHILIPPINES','code'=>'63'),
    	'PK'=>array('name'=>'PAKISTAN','code'=>'92'),
    	'PL'=>array('name'=>'POLAND','code'=>'48'),
    	'PM'=>array('name'=>'SAINT PIERRE AND MIQUELON','code'=>'508'),
    	'PN'=>array('name'=>'PITCAIRN','code'=>'870'),
    	'PR'=>array('name'=>'PUERTO RICO','code'=>'1'),
    	'PT'=>array('name'=>'PORTUGAL','code'=>'351'),
    	'PW'=>array('name'=>'PALAU','code'=>'680'),
    	'PY'=>array('name'=>'PARAGUAY','code'=>'595'),
    	'QA'=>array('name'=>'QATAR','code'=>'974'),
    	'RO'=>array('name'=>'ROMANIA','code'=>'40'),
    	'RS'=>array('name'=>'SERBIA','code'=>'381'),
    	'RU'=>array('name'=>'RUSSIAN FEDERATION','code'=>'7'),
    	'RW'=>array('name'=>'RWANDA','code'=>'250'),
    	'SA'=>array('name'=>'SAUDI ARABIA','code'=>'966'),
    	'SB'=>array('name'=>'SOLOMON ISLANDS','code'=>'677'),
    	'SC'=>array('name'=>'SEYCHELLES','code'=>'248'),
    	'SD'=>array('name'=>'SUDAN','code'=>'249'),
    	'SE'=>array('name'=>'SWEDEN','code'=>'46'),
    	'SG'=>array('name'=>'SINGAPORE','code'=>'65'),
    	'SH'=>array('name'=>'SAINT HELENA','code'=>'290'),
    	'SI'=>array('name'=>'SLOVENIA','code'=>'386'),
    	'SK'=>array('name'=>'SLOVAKIA','code'=>'421'),
    	'SL'=>array('name'=>'SIERRA LEONE','code'=>'232'),
    	'SM'=>array('name'=>'SAN MARINO','code'=>'378'),
    	'SN'=>array('name'=>'SENEGAL','code'=>'221'),
    	'SO'=>array('name'=>'SOMALIA','code'=>'252'),
    	'SR'=>array('name'=>'SURINAME','code'=>'597'),
    	'ST'=>array('name'=>'SAO TOME AND PRINCIPE','code'=>'239'),
    	'SV'=>array('name'=>'EL SALVADOR','code'=>'503'),
    	'SY'=>array('name'=>'SYRIAN ARAB REPUBLIC','code'=>'963'),
    	'SZ'=>array('name'=>'SWAZILAND','code'=>'268'),
    	'TC'=>array('name'=>'TURKS AND CAICOS ISLANDS','code'=>'1649'),
    	'TD'=>array('name'=>'CHAD','code'=>'235'),
    	'TG'=>array('name'=>'TOGO','code'=>'228'),
    	'TH'=>array('name'=>'THAILAND','code'=>'66'),
    	'TJ'=>array('name'=>'TAJIKISTAN','code'=>'992'),
    	'TK'=>array('name'=>'TOKELAU','code'=>'690'),
    	'TL'=>array('name'=>'TIMOR-LESTE','code'=>'670'),
    	'TM'=>array('name'=>'TURKMENISTAN','code'=>'993'),
    	'TN'=>array('name'=>'TUNISIA','code'=>'216'),
    	'TO'=>array('name'=>'TONGA','code'=>'676'),
    	'TR'=>array('name'=>'TURKEY','code'=>'90'),
    	'TT'=>array('name'=>'TRINIDAD AND TOBAGO','code'=>'1868'),
    	'TV'=>array('name'=>'TUVALU','code'=>'688'),
    	'TW'=>array('name'=>'TAIWAN, PROVINCE OF CHINA','code'=>'886'),
    	'TZ'=>array('name'=>'TANZANIA, UNITED REPUBLIC OF','code'=>'255'),
    	'UA'=>array('name'=>'UKRAINE','code'=>'380'),
    	'UG'=>array('name'=>'UGANDA','code'=>'256'),
    	'US'=>array('name'=>'UNITED STATES','code'=>'1'),
    	'UY'=>array('name'=>'URUGUAY','code'=>'598'),
    	'UZ'=>array('name'=>'UZBEKISTAN','code'=>'998'),
    	'VA'=>array('name'=>'HOLY SEE (VATICAN CITY STATE)','code'=>'39'),
    	'VC'=>array('name'=>'SAINT VINCENT AND THE GRENADINES','code'=>'1784'),
    	'VE'=>array('name'=>'VENEZUELA','code'=>'58'),
    	'VG'=>array('name'=>'VIRGIN ISLANDS, BRITISH','code'=>'1284'),
    	'VI'=>array('name'=>'VIRGIN ISLANDS, U.S.','code'=>'1340'),
    	'VN'=>array('name'=>'VIET NAM','code'=>'84'),
    	'VU'=>array('name'=>'VANUATU','code'=>'678'),
    	'WF'=>array('name'=>'WALLIS AND FUTUNA','code'=>'681'),
    	'WS'=>array('name'=>'SAMOA','code'=>'685'),
    	'XK'=>array('name'=>'KOSOVO','code'=>'381'),
    	'YE'=>array('name'=>'YEMEN','code'=>'967'),
    	'YT'=>array('name'=>'MAYOTTE','code'=>'262'),
    	'ZA'=>array('name'=>'SOUTH AFRICA','code'=>'27'),
    	'ZM'=>array('name'=>'ZAMBIA','code'=>'260'),
    	'ZW'=>array('name'=>'ZIMBABWE','code'=>'263')
    );
        
        
        
        if($request->FormType == "SendMobileVerification"){
            $number = $request->phoneNumberNational;
            $code = $request->phoneNumberCode;
            $CountryNumberCode = $countryArray[$code]['code'];
        
            $authy_api = new Authy\AuthyApi(env('TWILLO_AUTHY_API_KEY'));
            $res = $authy_api->phoneVerificationStart($number, $CountryNumberCode , 'sms');
            echo $res->message();
        }
        
        
        else if($request->FormType == "SendEmailVerification"){
            
              Mail::send(new imCoachEmailVerMail());
              echo "Email Will Send , ".$request->email;
           
        }
        
        else if($request->FormType == "IsMobileVerCodeCheck"){
            $number = $request->phoneNumberNational;
            $code = $request->phoneNumberCode;
            $CountryNumberCode = $countryArray[$code]['code'];
             $authy_api = new Authy\AuthyApi('e9gK7dUL35THTgEU2rJKkK6rCLN8DSDU');
    
            $res = $authy_api->phoneVerificationCheck($number, $CountryNumberCode , $request->codeIs);
           
            if ($res->ok()) {
                
                $value2 = ImCoachUser::where('id', $request->userId)->update([
                'MobileVerificationCode' => "0" ,
                "isMobileVerification"=> "1",
                'userstepsCompleted' => "2" ,
                ]);
                
                $json = json_encode(array('status'=>1 , 'link'=>url('imcoach/log') ,'mess'=>$res->message() ));
                
                 return $json;      
                
                
            }else{
                $json = json_encode(array('status'=>0 , 'link'=>url('imcoach/reg/1') ,'mess'=>$res->message() ));
                return $json;
            }

        }
        
        
    }catch(\Exception $e){
        echo $e;
    }
    
});

Route::get('imcoach/forgotpass', 'ImCoachUserController@ForgotPassFormGet');
Route::get('imcoach/mailVerify/{id}', function($id){
    $code = base64_decode($id);
    $value = ImCoachUser::where(['EmailVerificationCode' => $code])->first();
    if(count($value)>=1){
        $userid= $value->id;
        
        $value2 = ImCoachUser::where('id', $userid)->update([
                'EmailVerificationCode' => "0" ,
                "isEmailVerification"=> "1",
                'userstepsCompleted' => "2" ,
                ]);
        if($value2){
             return Redirect::to('imcoach/log');      
        }
        else{
            return redirect()->back();
        }
       
    }
    else{
        return Redirect::to('/');
    }
   
});
Route::post('imcoach/forgotpasstry', 'ImCoachUserController@ForgotPassFormPost');
Route::get('imcoach/mailforgotPass/{id}', function($id){
    echo $id;
});


//End login system part of i m coach





//this is the profile part of findcoach
Route::get('profile/findcoach/home', 'FindCouch_ProfileController@Home');
Route::get('profile/findcoach/editProfile', 'FindCouch_ProfileController@editProfile');
Route::get('profile/findcoach/payment', 'FindCouch_ProfileController@paymentGet');
Route::post('profile/findcoach/paymenttry', 'FindCouch_ProfileController@paymentPost');
Route::post('profile/findcoach/paymenttry2', 'FindCouch_ProfileController@paymentPreferPost');
Route::get('profile/findcoach/coaches', 'FindCouch_ProfileController@coachesGet');
Route::post('profile/findcoach/coachesFind', 'FindCouch_ProfileController@coachesFind');
Route::get('profile/findcoach/coachesFind/{activityOrHour}', 'FindCouch_ProfileController@coachesFindHour');
Route::get('profile/findcoach/coachesFind/{activity}/{hour}','FindCouch_ProfileController@coachesFindBoth');
Route::post('profile/findcoach/bookingcoachtry','FindCouch_ProfileController@BookingCoachTry');
Route::get('profile/findcoach/bookingcoachlist', 'FindCouch_ProfileController@bookingcoachlistGet');
Route::get('profile/findcoach/bookingcoachSessioncomplete', 'FindCouch_ProfileController@bookingcoachSessioncompleteGet');
Route::post('profile/findcoach/bookingcoachAvabiletry','FindCouch_ProfileController@BookingCoachAvableTry');
Route::get('profile/findcoach/PaymentPaypal', 'FindCouch_ProfileController@PayPalPaymentGet');

Route::post('profile/findcoach/editProfiletry', 'FindCouch_ProfileController@editProfilePost');

Route::get('profile/findcoach/admin', function(){
    echo "Under Development";
});




Route::get('profile/imcoach/home', 'IMCouch_ProfileController@home');
Route::get('profile/imcoach/editProfile', 'IMCouch_ProfileController@editProfile');
Route::post('profile/imcoach/editProfiletry', 'IMCouch_ProfileController@editProfilePost');
Route::get('profile/imcoach/booking', 'IMCouch_ProfileController@bookingGet');

Route::get('imcoach/logout', function(){
    
    session()->forget('ImCoachSuperUserInfo_Session');
   
    $cookie = Cookie::forget("ImCoachUserInfo_cookie");
    return Redirect::to('/')->withCookie($cookie);
});


Route::get('/test', function(){
    /*
    $sid = 'AC479b41c7ca02baf07ebc7158f049492e';
    $token = 'b8390d7ff739b1b5efeef13e3e7b73f4';
    $twilio = new Client($sid, $token);
    $phone_number = $twilio->lookups->v1->phoneNumbers("+8801521428741")->fetch();
    echo "<pre>";
    print_r($phone_number);
    echo "</pre>";
    
*/

    //e9gK7dUL35THTgEU2rJKkK6rCLN8DSDU
    //NE4pUp2bURP6Ljw1izzLJcN7dtxlNvd1
    /*
    $authy_api = new Authy\AuthyApi('e9gK7dUL35THTgEU2rJKkK6rCLN8DSDU');
    //$res = $authy_api->phoneVerificationStart('01521428741', '+880', 'sms');
     $res = $authy_api->phoneVerificationCheck('1521428741', '+880', '9360');
    if ($res->ok()) {
        print $res->message();
    }
    echo "<pre>";
    print_r($res);
    echo "<pre>";
    
    */

   /*
           $count =FindCoachUser::count();
           $UserFindCoach = new FindCoachUser;
           $UserFindCoach->firstname = "asdasd";
           $UserFindCoach->lastname = "asdasd";
           $UserFindCoach->email = "2163";
           $UserFindCoach->password = Hash::make("asdasdasf");
           $UserFindCoach->phonenumber = "jadasd";
           $UserFindCoach->birthday ="asdasd";
           $UserFindCoach->sport_interest = "asd,safs,sdf";
           $UserFindCoach->location = "asdasd";
           $UserFindCoach->isverifyemailstatus = "0";
           $val = "sportme_".str_random(21)."_".($count+1)."_1";
           $UserFindCoach->remember_token = $val;
           
           $UserFindCoach->save();
           
           Mail::send(new SendMail(
               emailvari));
           
           
           
           $arr = array("status" => "1" ,"link" => url('login')); 
           return json_encode($arr);
        
  */
   
    
});