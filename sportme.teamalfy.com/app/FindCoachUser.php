<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FindCoachUser extends Model
{
    public function booking()
    {
        return $this->hasMany(CoachBooking::class , 'find_coach_users_id')->orderBy('id', 'DESC');
    }
    
}
