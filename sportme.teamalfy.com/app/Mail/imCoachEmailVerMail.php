<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class imCoachEmailVerMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $val )
    {
        if($val->form_type == "imCoatch_User_emailVerify"){
            return $this->view('mail.imcoach.emailverimail',['req' => $val])->to($val->email)->from('noreplay@sportme.teamalfy.com');
         }
         else if($val->form_type == "imCoatch_User_forgetPassMailSend"){
            return $this->view('mail.imcoach.passverimail',['req' => $val])->to($val->email)->from('noreplay@sportme.teamalfy.com');
         }
    }
}
