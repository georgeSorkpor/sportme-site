<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendMail extends Mailable
{
    

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $token;
    public function __construct($tok)
    {
        $this->token = $tok;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $val)
    {
        
            return $this->view('mail.findcoach',['mes' => $this->token,'req' => $val ])->to($val->email)->from('support@sportme.teamalfy.com');
        
        
    }
}
