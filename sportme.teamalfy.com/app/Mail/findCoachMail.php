<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class findCoachMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $val)
    {
        if($val->form_type == "findcoatch_forgotpass"){
            return $this->view('mail.findcoatch.forgotpassmail',['req' => $val])->to($val->email)->from('support@sportme.teamalfy.com');
        }
       
    }
}
