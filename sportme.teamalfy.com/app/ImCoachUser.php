<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImCoachUser extends Model
{
    public function booking()
    {
        return $this->hasMany(CoachBooking::class , 'im_coach_users_id')->orderBy('id', 'DESC');
    }
}
