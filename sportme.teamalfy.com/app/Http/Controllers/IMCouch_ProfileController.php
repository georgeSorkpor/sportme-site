<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImCoachUser;
use Twilio\Rest\Client;
use Authy\AuthyApi;
use Redirect;
use Cookie;

class IMCouch_ProfileController extends Controller
{
     public function home()
     {
         
         
         
        if(session()->has('ImCoachSuperUserInfo_Session')){
            $id = session()->get('ImCoachSuperUserInfo_Session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 2){
                return view("profile.imcoach.home",['User_Values' => $user_info ,'session_info'=>$id]);
            }
            else{
                return Redirect::to('imcoach/logout');
            }
        }
         
         
        if(Cookie::has("ImCoachUserInfo_cookie")){
            $id = Cookie::get('ImCoachUserInfo_cookie');
            $value = explode("_",$id);
         
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if($user_info !== null && Cookie::has("ImCoachUserInfo_cookie") && $value[0] == 2 ){
                return view("profile.imcoach.home",['User_Values' => $user_info ,'cookie_info'=>$id]);
            }
            else{
                 return Redirect::to('imcoach/logout');
            }
         
         
        }else{
             return Redirect::to('imcoach/logout');
        }
        
    }
    public function editProfile()
    {
        
        
        if(session()->has('ImCoachSuperUserInfo_Session')){
            $id = session()->get('ImCoachSuperUserInfo_Session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 2){
                return view("profile.imcoach.editprofile",['User_Values' => $user_info ,'session_info'=>$id]);
            }
            else{
                return Redirect::to('imcoach/logout');
            }
        }
        
        
        
        
        
        if(Cookie::has("ImCoachUserInfo_cookie")){
            $id = Cookie::get('ImCoachUserInfo_cookie');
            $value = explode("_",$id);
         
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if($user_info !== null && Cookie::has("ImCoachUserInfo_cookie") && $value[0] == 2 ){
                
                return view("profile.imcoach.editprofile",['User_Values' => $user_info ,'cookie_info'=>$id]);
                
            }
            else{
                 return Redirect::to('imcoach/logout');
            }
         
         
        }else{
             return Redirect::to('imcoach/logout');
        }
        
    }
    
    
    public function editProfilePost(Request $request)
    {
        
        try{
                if(Cookie::has("ImCoachUserInfo_cookie")){
                    $id = Cookie::get('ImCoachUserInfo_cookie');
                    $value = explode("_",$id);
                    
                    //dd($request);
                    
                    $Fname = $request->Fname;
                    $Lname = $request->Lname;
                    $email = $request->Useremail;
                    $interest = $request->ImInterestCoach;
                    $phoneNumber = $request->PhoneNumber;
                    $aboutme = $request->AboutMe;
                    $userId = $request->UserId;
                    $FileTotalLink = null;
                    
                    
                    $sid = env('TWILLO_LOOKUP_ID');
                    $token = env('TWILLO_LOOKUP_TOKEN');
                    $twilio = new Client($sid, $token);
                    $phone_number = $twilio->lookups->v1->phoneNumbers($phoneNumber)->fetch();
                    
                    
                    
                    
                    
                    
                    
                    $file = $request->file('ImageFile');
                     if($file){
                         if($file->getSize() > 5000000){
                            return Redirect::to('profile/imcoach/editProfile')->withInput()->withErrors('File size is too big!!'); 
                         }
                         else{
                            
                            $fileName = 'images/imcoachs/profilepic/sportme_profilePic_2_'.$userId.$file->getClientOriginalName();
                            $ext = pathinfo(storage_path().$fileName, PATHINFO_EXTENSION);
                            $nameTestFile =  'sportme_profilePic_2_'.$userId."_".str_random(10)."_".$email."_profilePic.".$ext;
                            $FileTotalLink = '/images/imcoachs/profilepic/'.$nameTestFile;
                            $file->move('storage/images/imcoachs/profilepic/', $nameTestFile); 
                         }
                     }
                     else{
                         $FileTotalLink = null;
                     }
                    
                    
                    
                    
                   $value = ImCoachUser::where('id', $userId)->update([
                        'firstname' => $Fname,
                        'lastname' =>  $Lname,
                        'phonenumber' =>  $phoneNumber,
                        'CoachInterest' =>  $interest,
                        'aboutme' => $aboutme ,
                        'userimagelink' =>  $FileTotalLink,
                        
                        ]);
                    
                   
                   return Redirect::to('profile/imcoach/home'); 
                 
                 
                }else{
                     return Redirect::to('imcoach/logout');
                }
                
        }catch(\Exception  $e){
                echo get_class($e)."<br>";
                if(get_class($e) == 'Twilio\Exceptions\RestException'){
                    return Redirect::to('profile/imcoach/editProfile')->withInput()->withErrors('Check Your Mobile Number!!'); 
                }
                else{
                    return Redirect::to('profile/imcoach/editProfile')->withInput()->withErrors('Please Contuct us'); 
                    
                }
                 
        }
    
    }


    public function bookingGet()
     {
         
         
         
        if(session()->has('ImCoachSuperUserInfo_Session')){
            $id = session()->get('ImCoachSuperUserInfo_Session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 2){
                return view("profile.imcoach.booking",['User_Values' => $user_info ,'session_info'=>$id]);
            }
            else{
                return Redirect::to('imcoach/logout');
            }
        }
         
         
        if(Cookie::has("ImCoachUserInfo_cookie")){
            $id = Cookie::get('ImCoachUserInfo_cookie');
            $value = explode("_",$id);
         
            $user_info = ImCoachUser::where('id',$value[1])->first();
            
            if($user_info !== null && Cookie::has("ImCoachUserInfo_cookie") && $value[0] == 2 ){
                return view("profile.imcoach.booking",['User_Values' => $user_info ,'cookie_info'=>$id]);
            }
            else{
                 return Redirect::to('imcoach/logout');
            }
         
         
        }else{
             return Redirect::to('imcoach/logout');
        }
        
    }

}
