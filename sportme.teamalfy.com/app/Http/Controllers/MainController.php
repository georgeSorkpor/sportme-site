<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FindCoachUser;
use App\ImCoachUser;
Use Redirect ;
use Cookie;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * home page method
     */
    public function home()
    {
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
        
            
            if(count($user_info)>=1 && $value[0] == 1){
                return Redirect::to("profile/findcoach/home");
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }
        else if(session()->has('ImCoachSuperUserInfo_Session')){
            $id = session()->get('ImCoachSuperUserInfo_Session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = ImCoachUser::where('id',$value[1])->first();
        
            
            if(count($user_info)>=1 && $value[0] == 2){
                return Redirect::to('profile/imcoach/home');
            }
            else{
                return Redirect::to('imcoach/logout');
            }
        }
        
        
        if(\Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
             $value = explode("_",$id);
             if($value[0] == 1){
                 return Redirect::to('profile/findcoach/home');
             }
             else if($value[0] == 2){
                 echo "Under development";
             }
             
        }
        
        else if(\Cookie::has("ImCoachUserInfo_cookie")){
            $id = Cookie::get('ImCoachUserInfo_cookie');
             $value = explode("_",$id);
             if($value[0] == 2){
                 return Redirect::to('profile/imcoach/home');
             }
             
        }
        else{
            return view('home');
        }
        
            
        
        
    }
    
     /**
     * Display a listing of the resource.
     *
     * about page method
     */
    
    public function about()
    {
        
        try{
            return view('about');
        }catch(\Exception $e){
            echo $e ;
        }
        
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * contactus page method
     */
    public function contactus()
    {
        
        try{
            return view('contactus');
        }catch(\Exception $e){
            echo $e ;
        }
        
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * login page method
     */
    public function login()
    {
      /*  
        try{
            return view('login');
        }catch(\Exception $e){
            echo $e ;
        }
        */
        
    }
    
    
     
    /**
     * Display a listing of the resource.
     *
     * careers page method
     */
    public function careers()
    {
        
        try{
            return view('careers');
        }catch(\Exception $e){
            echo $e ;
        }
        
    }
    
    
   
}
