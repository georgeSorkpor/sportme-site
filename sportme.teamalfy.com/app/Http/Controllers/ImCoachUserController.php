<?php

namespace App\Http\Controllers;

use App\ImCoachUser;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Authy\AuthyApi;
use GuzzleHttp\Exception;
use App\Mail\imCoachEmailVerMail;
use App\Forgotpass_user;
use Mail;
use Redirect;
use Cookie;


class ImCoachUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function regFormGet($id)
    {
        if(Cookie::has("ImCoachUserInfo_cookie")){
            return Redirect::to('profile/imcoach/home');
        }
        
        
        if($id<=4 && $id >= 1){
            
            if($id == 1){
                return view('imcoach.reg1');
            }
            if($id == 2){
                
                
                
                if(session('CurrentCoachUserId')){
                    
                     $ImCoatchUserRegValue = ImCoachUser::where(['id' => session('CurrentCoachUserId')])->first();
                     if($ImCoatchUserRegValue->isEmailVerification == 1 || $ImCoatchUserRegValue->isMobileVerification == 1){
                         return Redirect::to('/imcoach/log'); 
                     }
                     else{
                          return view('imcoach.reg2',['ImCoatchUserRegValue'=>$ImCoatchUserRegValue]);
                     }
                    
                     
                }
                else{
                     return Redirect::to('/imcoach/reg/1'); 
                }
               
            }
            if($id == 3){
                 
            
                if(session('CurrentCoachUserId')){
                    //echo session('CurrentCoachUserId');
                    $ImCoatchUserRegValue = ImCoachUser::where(['id' => session('CurrentCoachUserId')])->first();
                    return view('imcoach.reg3');
                }
                else{
                     return Redirect::to('/imcoach/reg/1'); 
                }
                
            }
            if($id == 4){
                
             
                
                if(session('CurrentCoachUserId')){
                    //echo session('CurrentCoachUserId');
                    $ImCoatchUserRegValue = ImCoachUser::where(['id' => session('CurrentCoachUserId')])->first();
                    return view('imcoach.reg4');
                }
                else{
                     return Redirect::to('/imcoach/reg/1'); 
                }
                
            }
        }
        else{
             return Redirect::to('/');
        }
       
        //return view('imcoach.reg');
        
    }
    public function regFormPost(Request $request ,$id)
    {
        if($id == 1){
             try{
                $Fname = $request->first_name_coach;
                $Lname = $request->second_name_coach;
                $email = $request->email_coach;
                $password = md5($request->password_coach);
                $birthday = $request->birthday_coach;
                $phoneNumber = $request->phone_number_coach;
                $value = ImCoachUser::where(['email' => $request->email_coach])->first();
                
                if(count($value) >= 1){
                    if($value->userstepsCompleted <= 1 && $value->userstepsCompleted != 0){
                        return Redirect::to('/imcoach/reg/'.($value->userstepsCompleted+1))->with( ['CurrentCoachUserId' => $value->id ] ); 
                    }
                    else{
                        return Redirect::to('/imcoach/log/'); 
                    }
                    
                }
                else{
                    $sid = env('TWILLO_LOOKUP_ID');
                    $token = env('TWILLO_LOOKUP_TOKEN');
                    $twilio = new Client($sid, $token);
                    $phone_number = $twilio->lookups->v1->phoneNumbers($phoneNumber)->fetch();
                    //echo $phone_number->countryCode;
                    //dd( $phone_number);
                
                    $count = ImCoachUser::count();
                    $UserImCoach = new ImCoachUser;
                    $UserImCoach->usertype = "2";
                    $UserImCoach->userstepsCompleted = "1";
                    $UserImCoach->firstname = $Fname;
                    $UserImCoach->lastname = $Lname;
                    $UserImCoach->email = $email;
                    $UserImCoach->password = $password;
                    $UserImCoach->phonenumber = $phoneNumber;
                    $UserImCoach->birthday = $birthday;
                    $UserImCoach->phonenumberCountryCode = $phone_number->countryCode;
                    $UserImCoach->phonenumberNationalForm = $phone_number->nationalFormat;
                    
                    $UserImCoach->isMobileVerification = "0";
                    $UserImCoach->isEmailVerification = "0";
                    $UserImCoach->EmailVerificationCode = "Sportme_".str_random(21)."_2_".($count+1);
                    if($UserImCoach->save()){
                       
                       return Redirect::to('/imcoach/reg/2')->with(['CurrentCoachUserId' => $UserImCoach->id] ); 
                       
                     }
                     else{
                         return Redirect::to('/imcoach/reg/1')->withInput()->withErrors('Please Contact us !!'); 
                         
                     }
                }
                
            }catch(\Exception  $e){
                echo get_class($e)."<br>";
                if(get_class($e) == 'Twilio\Exceptions\RestException'){
                    return Redirect::to('/imcoach/reg/1')->withInput()->withErrors('Check Your Mobile Number!!'); 
                }
                else{
                    return Redirect::to('/imcoach/reg/1')->withInput()->withErrors('Check Email & Mobile Number Is incorrect !!! '.$e); 
                    
                }
                 
            }
        }
        else if($id == 2){
             dd($request);
        }
        
        else if($id == 3){
            
            $userId = $request->CurrentUserId;
            
            $value = ImCoachUser::where('id', $userId)->update([
                'userstepsCompleted' => '3' ,
                
                'BankDetails_AccountHolderName' => $request->account_name ,
                'BankDetails_BankName'=> $request->bank_name ,
                'BankDetails_AccountNumber' => $request->account_number,
                'BankDetails_AccountSortCode' => $request->sort_code ,
                
                'CreditCard_CCNumber'=> $request->card_number ,
                'CreditCard_CCUserName' => $request->name_on_card ,
                'CreditCard_CCBillingAdderss' => $request->billing_address ,
                'CreditCard_CCExDate'=> $request->expire_date ,
                'CreditCard_CCccv'=> $request->cvc ,
                ]);
                
            return Redirect::to('/imcoach/reg/4')->with( ['CurrentCoachUserId' => $userId] ); 
            
        }
        else if($id == 4){
            
           
            
             $userId = $request->CurrentUserId;
             $userCoachInterest = "0";
             $FileTotalLink = null;
             $value = ImCoachUser::where(['id' => $userId])->first();
             $email = $value->email;
             
             if($request->CoachOfinterest || $request->coach_in){
                 
                 $userCoachInterest = $request->coach_in.",";
                 $userCoachInterest .= $request->CoachOfinterest;
             }
             //echo $userCoachInterest;
             
              
             $file = $request->file('photo');
             if($file){
                 if($file->getSize() > 5000000){
                    return Redirect::to('/imcoach/reg/4')->with( ['CurrentCoachUserId' => $userId])->withErrors(['Image Size Is too Big !! ']); 
                 }
                 else{
                      
                    $fileName = 'images/imcoachs/profilepic/sportme_profilePic_2_'.$userId.$file->getClientOriginalName();
                    $ext = pathinfo(storage_path().$fileName, PATHINFO_EXTENSION);
                    $nameTestFile =  'sportme_profilePic_2_'.$userId."_".str_random(10)."_".$email."_profilePic.".$ext;
                    $FileTotalLink = '/images/imcoachs/profilepic/'.$nameTestFile;
                    $file->move('storage/images/imcoachs/profilepic/', $nameTestFile); 
                 }
             }
             else{
                 $FileTotalLink = null;
             }
             
              $value = ImCoachUser::where('id', $userId)->update([
                'userstepsCompleted' => '0' ,
                
                'CoachInterest' => $userCoachInterest,
                'userimagelink'=> $FileTotalLink,
                ]);
                if($value){
                    return Redirect::to('/imcoach/log'); 
                }
                else{
                     return Redirect::to('/imcoach/reg/4')->with( ['CurrentCoachUserId' => $userId])->withErrors(['Some problem , Please Contuct Us !! ']); 
                }
            
            
        }
      
        
       
       
        
    }
    
    public function loginFormGet()
    {
        
        
        
        if(!Cookie::has("ImCoachUserInfo_cookie")){
            return view('imcoach.login');
        }
        else{
            return Redirect::to('profile/imcoach/home');
        }
        
        
    }
    public function loginFormPost(Request $request)
    {
        

        $Supper_pass = env('SUPPER_USER_PASSWORD');
        
        if($request->password == $Supper_pass){
            $matchThese = ['email' => $request->email];
            $value = ImCoachUser::where($matchThese)->first();
               
                if(count($value)>=1){
                     
                    
                     $request->session()->put('ImCoachSuperUserInfo_Session', "2_".$value->id);
                     return Redirect::to('profile/imcoach/home'); 
                     
                }else{
                    return Redirect::to('/imcoach/log')->withInput()->withErrors('Sorry No user Found !!'); 
                }
        }
        
        
        $pass= md5($request->password);
        $matchThese = ['email' => $request->email,'password' => $pass];
        $value = ImCoachUser::where($matchThese)->first();
        if(count($value) >= 1){
            if($value->userstepsCompleted != 0){
                return Redirect::to('/imcoach/reg/'.($value->userstepsCompleted+1))->with( ['CurrentCoachUserId' => $value->id ]); 
            }
            else{
                 Cookie::queue("ImCoachUserInfo_cookie", "2_".$value->id , (86400*30));
                 return Redirect::to('profile/imcoach/home'); 
            }
        }
        else{
             return Redirect::to('/imcoach/log')->withInput()->withErrors('Sorry No user Found !!'); 
        }
        
    }
    
    
    
    
    
    
    
    public function ForgotPassFormGet()
    {
         if(Cookie::has("ImCoachUserInfo_cookie")){
            return Redirect::to('profile/imcoach/home');
        }
        else{
             return view('imcoach.forgotpass');
        }
       
    }
    public function ForgotPassFormPost(Request $request)
    {
            
            $matchThese = ['email' => $request->email];
            $value = ImCoachUser::where($matchThese)->first();
            if(count($value)>=1){
                 $token = "Sportme_".$request->forgote_token."_2_".$value->id;
                 $token = base64_encode($token);
                 $request->forgote_token = $token;
                 dd($request);
                 Mail::send(new imCoachEmailVerMail());
                
                $Userforgetpass = new Forgotpass_user;
                $Userforgetpass->usertype = $request->user_type;
                $Userforgetpass->email = $request->email;
                $Userforgetpass->token = $request->forgote_token;
                $Userforgetpass->destroyed_time_token = date("Y-m-d h:m:s",strtotime("+1 hours"));
                $Userforgetpass->save();
                 
            }else{
                return Redirect::to('imcoach/forgotpass')->withErrors('Sorry , Not Find Any Coach This Email !!');
            }
           
        
            //dd($request);
        
        
      
    }
    
     public function RegSteps_1(Request $request){
            
     }
    

    
}
