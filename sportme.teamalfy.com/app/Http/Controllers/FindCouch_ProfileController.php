<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Error\Card;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\FindCoachUser;
use App\CoachBooking;
use App\ImCoachUser;
Use Redirect;
use Cookie;




use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class findcouch_profileController extends Controller
{
    public $Paypal_Request;
    
    public function Home()
    {
        
        
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                return view("profile.findcoach.home",['User_Values' => $user_info ,'Session_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }
            
       
           
        
        
        
        
      if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
        
            if($user_info !== null && Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                return view("profile.findcoach.home",['User_Values' => $user_info ,'cookie_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
      }  
      else{
          return Redirect::to('findcoach/logout');
      }
        
        
       
    }
    
    
    public function editProfile()
    {
        
        
         if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                return view("profile.findcoach.editprofile",['User_Values' => $user_info ,'Session_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
            
        }
            
        
        
        
        
        
        if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
           
            if(Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                    $user_info = FindCoachUser::where('id',$value[1])->first();
                    if(count($user_info) >= 1){
                        return view("profile.findcoach.editprofile",['User_Values' => $user_info ,'cookie_info'=>$id]);
                    }
                    else{
                         return Redirect::to('findcoach/logout');
                    }
                    
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }else{
            return Redirect::to('findcoach/logout');
        }
        
        
        
    }
    
     public function editProfilePost(Request $request)
    {
        
            $id = $request->UserId;
            $email = $request->Useremail;
            $fname = $request->Fname;
            $lname = $request->Lname;
            $Workoutgoal = $request->WorkoutGoal;
            $aboutme = $request->AboutMe;
            $phone = $request->PhoneNumber;
            $file = $request->file('ImageFile');
            $filePathName = $request->ImageLink;
            
            if($file){
                
               
                $fileName = 'images/findcoachs/profilepic/sportme_profilePic_1_'.$id.$file->getClientOriginalName();
                $ext = pathinfo(storage_path().$fileName, PATHINFO_EXTENSION);
                $nameTestFile =  'sportme_profilePic_1_'.$id."_".str_random(10)."_".$email."_profilePic.".$ext;
                $filePathName = '/images/findcoachs/profilepic/'.$nameTestFile;
               
               
                $file->move('storage/images/findcoachs/profilepic/', $nameTestFile); 
                
                
            }
            
           FindCoachUser::where('id', $id)->update([
                'firstname' => $fname , 
                'lastname' => $lname , 
                'aboutme' => $aboutme,
                'workoutGoal' => $Workoutgoal,
                'phonenumber' => $phone,
                "userimagelink"=>$filePathName
                ]);
           
            return Redirect::to('profile/findcoach/home');
       
    }
    
    
    
    public function coachesGet()
    {
        
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                $value = ImCoachUser::where('isMobileVerification' , '1')->orWhere('isEmailVerification' , '1')->get();
                return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$value]);
        
            }
            else{
                return Redirect::to('findcoach/logout');
            }
            
        }
      
        
        if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
           
            if(Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                    $user_info = FindCoachUser::where('id',$value[1])->first();
                    if(count($user_info) >= 1){
                        $value = ImCoachUser::where('isMobileVerification' , '1')->orWhere('isEmailVerification' , '1')->get();
                        return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$value]);
                        
                    }
                    else{
                         return Redirect::to('findcoach/logout');
                    }
                    
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }else{
            return Redirect::to('findcoach/logout');
        }
        
    }
    
    public function coachesFind(Request $request){
        
        if($request->ActivityTypeCoach == null && $request->PerHourRate == 0 ){
             return Redirect::to('profile/findcoach/coaches');
        }
        $ActivityTypeCoach = $request->ActivityTypeCoach;
        $PerHourRate = $request->PerHourRate;
        if($ActivityTypeCoach != null && $PerHourRate != null){
             return Redirect::to('profile/findcoach/coachesFind/'.$ActivityTypeCoach.'/'.$PerHourRate);
        }
        else if($ActivityTypeCoach == null && $PerHourRate != null){
            return Redirect::to('profile/findcoach/coachesFind/'.$PerHourRate);
        }
        
       
    }
    public function coachesFindHour($activityOrHour){
        
       if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                $userSearch = ImCoachUser::where('parhourCost','LIKE',$activityOrHour.'%')->get();
                return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$userSearch]);
        
            }
            else{
                return Redirect::to('findcoach/logout');
            }
            
        }
      
        
        if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
           
            if(Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                    $user_info = FindCoachUser::where('id',$value[1])->first();
                    if(count($user_info) >= 1){
                        
                         
                        $userSearch = ImCoachUser::where('parhourCost','LIKE',$activityOrHour.'%')->get();
                        return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$userSearch]);
                        
                    }
                    else{
                         return Redirect::to('findcoach/logout');
                    }
                    
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }else{
            return Redirect::to('findcoach/logout');
        }
        
       
       
    
        
       
    }
    public function coachesFindBoth($activity,$hour){
        
        
        
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                $userSearch ;
                if($hour == 0){
                        $userSearch = ImCoachUser::where('CoachInterest','LIKE','%'.$activity.'%')->get();
                }
                else{
                    $userSearch = ImCoachUser::where('CoachInterest','LIKE','%'.$activity.'%')->where('parhourCost','LIKE',$hour."%")->get();
                }
                return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$userSearch]);
        
            }
            else{
                return Redirect::to('findcoach/logout');
            }
            
        }
      
        
        if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
           
            if(Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                    $user_info = FindCoachUser::where('id',$value[1])->first();
                    if(count($user_info) >= 1){
                        
                         $userSearch ;
                         if($hour == 0){
                             $userSearch = ImCoachUser::where('CoachInterest','LIKE','%'.$activity.'%')->get();
                         }
                         else{
                             $userSearch = ImCoachUser::where('CoachInterest','LIKE','%'.$activity.'%')->orWhere('parhourCost','LIKE',$hour."%")->get();
                         }
                        return view("profile.findcoach.coachbooking",['User_Values' => $user_info , 'All_Coachs'=>$userSearch]);
                        
                    }
                    else{
                         return Redirect::to('findcoach/logout');
                    }
                    
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }else{
            return Redirect::to('findcoach/logout');
        }
        
        
    }
    
    
    
    
    public function paymentGet()
    {
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                
                return view("profile.findcoach.paymentPrefer",['User_Values' => $user_info ]);
        
            }
            else{
                return Redirect::to('findcoach/logout');
            }
            
        }
      
        
        if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
           
            if(Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                    $user_info = FindCoachUser::where('id',$value[1])->first();
                    if(count($user_info) >= 1){
                        
                        return view("profile.findcoach.paymentPrefer",['User_Values' => $user_info ]);
                       
                    }
                    else{
                         return Redirect::to('findcoach/logout');
                    }
                    
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }else{
            return Redirect::to('findcoach/logout');
        }
       
        
    }
    
    public function paymentPost(Request $request)
    { 
       
            
            $Name = $request->CCName;
            $number = $request->CCNumber;
            $Address = $request->CCAddress;
            $ExpDate = explode("/",$request->CCExpDate);
            $ccv = $request->Ccv;
            $id= $request->UserId;
            
            
            
            $stripe = Stripe::make(env('STRIPE_SECRET'));
            
            try{ 
            $token = $stripe->tokens()->create([
                 'card' => [
                 'number' => $number,
                 'exp_month' => $ExpDate[0],
                 'exp_year' => $ExpDate[1],
                 'cvc' => $ccv
                 ],
             ]);
            
            if($token){
                FindCoachUser::where('id', $id)->update([
                    'CreditCard_CCNumber' => $number , 
                    'CreditCard_CCUserName' => $Name , 
                    'CreditCard_CCBillingAdderss' => $Address,
                    'CreditCard_CCExDate' => $request->CCExpDate,
                    'CreditCard_CCccv' => $ccv
                ]);
                 return Redirect::to('profile/findcoach/payment')->withErrors(["Update Card Information Successfully"]);
            }
            else{
                 return Redirect::to('profile/findcoach/payment')->withErrors(['Sorry , Try Again !!']);
            }
            
            
            
        }catch(\Exception $e){

            return Redirect::to('profile/findcoach/payment')->withErrors([$e->getMessage()]);
            
        }
       
           
    }
    public function paymentPreferPost(Request $request){
         if( $request->checkbox  == 0){
             $id = $request->id;
              FindCoachUser::where('id', $id)->update([
                'PaymentPrafer'=>$request->checkbox
            ]);
                
         }
         else if($request->checkbox  == 1 ){
             $id = $request->id;
              FindCoachUser::where('id', $id)->update([
                'PaymentPrafer'=>$request->checkbox
            ]);
         }
    }
    
    
    
     public function BookingCoachTry(Request $request){
         
         try{
             
              $DateOfSession = $request->DateOfBookingCoach;
             
             
             
             
             
             
            
             $StartSession = strtotime($request->StartTimeOfBookingCoach);
             $endSession = strtotime($request->EndTimeOfBookingCoach);
             
             $diff = ($endSession - $StartSession)/3600;
             $allBookingtime = array();
             $startingDate = explode(":",$request->StartTimeOfBookingCoach);
             array_push($allBookingtime,$startingDate[0].":00:00");
             for( $i = 1;$i <= $diff;$i++){
                 $val =  sprintf("%02d", $startingDate[0] + $i);
                 array_push($allBookingtime,$val.":00:00");
             }
             $totalTime = "";
             foreach($allBookingtime as $time){
                 $totalTime .= $time.",";
                 
             }
             
              $value = CoachBooking::where('im_coach_users_id',$request->BookingCoachId)->where('BookingDate',$DateOfSession)->get();
              foreach($value as $test){
                  $array = explode(",",$test->BookingTime);
                  foreach($allBookingtime as $Bookintimetest){
                      if (in_array($Bookintimetest, $array))
                      {
                          $value_new = CoachBooking::where('im_coach_users_id',$request->BookingCoachId)->where('BookingDate',$DateOfSession)->get();
                          $totalTime = "";
                          foreach($value_new as $value){
                              $totalTime .= $value->BookingTime;
                          }
                            $mes = json_encode(array('status'=>'error' , 'message'=>str_replace(","," , ",$totalTime)));
                            echo $mes;
                            return;
                      }
                    
                  }
              }
             //echo $totalTime;
             //echo date('h:m:s' ,$StartSession )."  ".date('h:m:s' ,$endSession )." ".$diff;
             
            
            
             
             if($request->UserBookingPrefer == 0){
               
                $bookingUserId =FindCoachUser::where('id',$request->BookingUserId)->first();
                $bookingCoachinfo =ImCoachUser::where('id',$request->BookingCoachId)->first();
                
                
                //sk_test_BZgcrdGKVbfwrvoY2bmz5maI
                $stripe = Stripe::make(env('STRIPE_SECRET'));
                //$stripe = Stripe::make('sk_test_BZgcrdGKVbfwrvoY2bmz5maI');
                $Name = $request->CCName;
                $number = $request->CCNumber;
                $Address = $request->CCAddress;
                $ExpDate = explode("/",$request->CCExpDate);
                $ccv = $request->Ccv;
            
               
                
                
                if($Name != null && $number != null && $Address != null && $ExpDate != null && $ccv != null ){
                         $token = $stripe->tokens()->create([
                             'card' => [
                             'number' => $number,
                             'exp_month' => $ExpDate[0],
                             'exp_year' => $ExpDate[1],
                             'cvc' => $ccv,
                             'address_line1'=>$Address
                             ],
                        ]);
                        
                        if (isset($token['id'])) {
                            
                            $stripe = Stripe::make(env('STRIPE_SECRET'));
                            $charge = $stripe->charges()->create([
                                'card' => $token['id'],
                                'currency' => 'GBP',
                                'amount'   => $request->CoachParHourCost * $diff,
                                'metadata' => [
                                     "Coach_Id" => $request->BookingCoachId,
                                     "Coach_Email" => $bookingCoachinfo->email,
                                     "Coach_PerHourPrice" => $bookingCoachinfo->parhourCost,
                                     "Coach_Amount_Is_Paid_Admin" => false,
                                     
                                     "FindCoach_Id" => $request->BookingUserId,
                                     "FindCoach_Email" => $bookingUserId->email,
                                     "FindCoach_CCName" => $Name,
                                     "FindCoach_CCNumber" => $number,
                                     "FindCoach_CCAddress" => $Address,
                                     "FindCoach_CCExpDate" => $ExpDate[0]."/".$ExpDate[1],
                                     "FindCoach_CVC" => $ccv,
                                     "FindCoach_Amount_Is_Paid_Admin" => true,
                                     
                                     "Booking_LocationPlace"=>$request->BookingMeetingLocation,
                                     "Booking_SessionDate"=>$DateOfSession,
                                     "Booking_SessionStartTime"=>$request->StartTimeOfBookingCoach,
                                     "Booking_SessionEndTime"=>$request->EndTimeOfBookingCoach,
                                     "Booking_TotalHour"=>$diff,
                                     "Booking_PerHourPrice"=>$request->CoachParHourCost,
                                    
                                     
                                     ]
                                 
                            ]);
                            
                           
                                $coachbooking = new CoachBooking;
                                $coachbooking->im_coach_users_id = $request->BookingCoachId;
                                $coachbooking->find_coach_users_id = $request->BookingUserId;
                                $coachbooking->BookingDate = $DateOfSession;
                                $coachbooking->BookingTime = $totalTime;
                                $coachbooking->BookingTotalHours = $diff;
                                $coachbooking->ParhourCoachAmount = $request->CoachParHourCost;
                                $coachbooking->totalAmount = ($request->CoachParHourCost * $diff);
                                $coachbooking->Location = $request->BookingMeetingLocation;
                                $coachbooking->PaymentWayFindCoach = $request->UserBookingPrefer;
                                $coachbooking->PaymentFindCoachAllInfo = json_encode($charge);
                                $coachbooking->isReviewMessageSend = "0";
                                $coachbooking->isSessionComplete = "0";
                                
                                if($coachbooking->save()){
                                      $mes = json_encode(array('status'=>'success' , 'message'=>'OK','link'=>url('profile/findcoach/payment')));
                                    echo $mes;
                                }
                                else{
                                      $mes = json_encode(array('status'=>'error' , 'message'=>' Sorry ! Not Data Enter in database','link'=>url('profile/findcoach/payment')));
                                    echo $mes;
                                }
                       
                     
                                
                                
                              
                       
                        }
                        else{
                            $mes = json_encode(array('status'=>'error' , 'message'=>'Sorry ! Token Does not Found ','link'=>url('profile/findcoach/payment')));
                            echo $mes;
                        }
                        
                }
                else{
                    $mes = json_encode(array('status'=>'error' , 'message'=>'Plaese Give The Correct Card Information','link'=>url('profile/findcoach/payment')));
                    echo $mes;
                }
               
                 
              
                 
             }
             else if($request->UserBookingPrefer == 1){
                 session_start();
                  $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        env("PAYPAL_CLIENT"),     // ClientID
                        env("PAYPAL_SECRET")      // ClientSecret
                        )
                    );
                      $apiContext->setConfig(array('mode' => env('PAYPAL_ENV'),));
                 
                    $payer = new \PayPal\Api\Payer();
                    $payer->setPaymentMethod('paypal');
                    
                    $coachValue = ImCoachUser::where('id',$request->BookingCoachId)->first();
                    
                  
                   
                    
                    $item1 = new Item();
                    $item1->setName($coachValue->firstname." ".$coachValue->lastname." Coach Booking")
                        ->setCurrency('GBP')
                        ->setQuantity(1)
                        ->setPrice($request->CoachParHourCost * $diff);
                        
                    $itemList = new ItemList();
                    $itemList->setItems(array($item1));
                    
                    
                
                    
                    $amount = new \PayPal\Api\Amount();
                    $amount->setTotal( $request->CoachParHourCost * $diff);
                     $amount->setCurrency('GBP');
                    
                    $transaction = new \PayPal\Api\Transaction();
                    $transaction->setAmount($amount)
                                ->setItemList($itemList)
                                ->setDescription('Your transaction description')
                                ->setInvoiceNumber(uniqid());
                   
                    $redirectUrls = new \PayPal\Api\RedirectUrls();
                    $redirectUrls->setReturnUrl(url('profile/findcoach/PaymentPaypal'))
                        ->setCancelUrl(url('profile/findcoach/PaymentPaypalCancel'));
                    
                  
                    
                    $InputFields = new \PayPal\Api\InputFields();
                    $InputFields->setNoShipping(1);
                    
                    $WebProfile = new \PayPal\Api\WebProfile();
                    $WebProfile->setName( "Sport Me PayPal ".uniqid() )->setInputFields($InputFields);
                    
                    $WebProfileid = $WebProfile->create($apiContext)->getId();
                    
                    $payment = new \PayPal\Api\Payment();
                    $payment->setExperienceProfileId( $WebProfileid );
                    $payment->setIntent('sale')
                        ->setPayer($payer)
                        ->setTransactions(array($transaction))
                        ->setRedirectUrls($redirectUrls);
                        
                        
                    if($payment->create($apiContext)){
                        \Cookie::queue("PAYPAL_BOOKING_COACH_ID", $request->BookingCoachId , 30);
                        \Cookie::queue("PAYPAL_BOOKING_FIND_USER_ID", $request->BookingUserId , 30);
                        \Cookie::queue("PAYPAL_BOOKING_DATE", $request->DateOfBookingCoach , 30);
                        \Cookie::queue("PAYPAL_BOOKING_TIME", $totalTime , 30);
                        \Cookie::queue("PAYPAL_BOOKING_TOTALHOUR", $diff , 30);
                        \Cookie::queue("PAYPAL_BOOKING_TOTALCOST", ($request->CoachParHourCost * $diff) , 30);
                        \Cookie::queue("PAYPAL_BOOKING_COACHPERHOURPRICE", $request->CoachParHourCost , 30);
                        \Cookie::queue("PAYPAL_BOOKING_LOCATION", $request->BookingMeetingLocation , 30);
                        /*
                        $this->Paypal_Request = $request;
                        \Session::put('PAYPAL_BOOKING_COACH_ID',$request->BookingCoachId);
                        $request->session()->put('PAYPAL_BOOKING_COACH_ID', $request->BookingCoachId);
                        $request->session()->put('PAYPAL_BOOKING_FIND_USER_ID', $request->BookingUserId);
                        $request->session()->put('PAYPAL_BOOKING_REQUEST_VALUE', $request);
                        $request->session()->put('PAYPAL_BOOKING_PAYPAL_PAYMENT_CREATE', $payment);
                        */
                       
                        $mes = json_encode(array('status'=>'success' , 'message'=>$payment->getApprovalLink()));
                        echo $mes;
                        
                    }else{
                        $mes = json_encode(array('status'=>'error' , 'message'=>"Sorry !  PayPal not working",'link'=>url('profile/findcoach/payment')));
                        echo $mes;
                    }
                      
                        
    
               
             }
         }catch(\Exception $e){
            $mes = json_encode(array('status'=>'error' , 'message'=>$e->getMessage(),'link'=>url('profile/findcoach/payment')));
            echo $mes;
         }
         
     }
    
    
     public function bookingcoachlistGet(){
         
         
          
        
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                return view("profile.findcoach.coachbookinglist",['User_Values' => $user_info ,'Session_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }
            
       
           
        
        
        
        
      if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
        
            if($user_info !== null && Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                return view("profile.findcoach.coachbookinglist",['User_Values' => $user_info ,'cookie_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
      }  
      else{
          return Redirect::to('findcoach/logout');
      }
        
        
         
         
         
         //$comments = CoachBooking::find("11")->FindCoach;
        //dd($comments);
   
    
     }
     
     
      public function bookingcoachSessioncompleteGet(){
         
         
          
         
        if(session()->has('FindCoachSuperUserInfo_session')){
            $id = session()->get('FindCoachSuperUserInfo_session');
            //echo $id;
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
            
            if(count($user_info)>=1 && $value[0] == 1){
                return view("profile.findcoach.coachSessioncomplete",['User_Values' => $user_info ,'Session_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
        }
            
       
           
        
        
        
        
      if(Cookie::has("FindCoachUserInfo_cookie")){
            $id = Cookie::get('FindCoachUserInfo_cookie');
            $value = explode("_",$id);
            $user_info = FindCoachUser::where('id',$value[1])->first();
        
            if($user_info !== null && Cookie::has("FindCoachUserInfo_cookie") && $value[0] == 1 ){
                return view("profile.findcoach.coachSessioncomplete",['User_Values' => $user_info ,'cookie_info'=>$id]);
            }
            else{
                return Redirect::to('findcoach/logout');
            }
      }  
      else{
          return Redirect::to('findcoach/logout');
      }
        
        
         
        
         
        
   
    
     }
    
    
    public function BookingCoachAvableTry(Request $request){
        try{
             $DateOfSession = $request->DateOfBookingCoach;
            
             $StartSession = strtotime($request->StartTimeOfBookingCoach);
             $endSession = strtotime($request->EndTimeOfBookingCoach);
             
             $diff = ($endSession - $StartSession)/3600;
             $allBookingtime = array();
             $startingDate = explode(":",$request->StartTimeOfBookingCoach);
             array_push($allBookingtime,$startingDate[0].":00:00");
             for( $i = 1;$i <= $diff;$i++){
                 $val =  sprintf("%02d", $startingDate[0] + $i);
                 array_push($allBookingtime,$val.":00:00");
             }
             $totalTime = "";
             foreach($allBookingtime as $time){
                 $totalTime .= $time.",";
                 
             }
             
              $value = CoachBooking::where('im_coach_users_id',$request->BookingCoachId)->where('BookingDate',$DateOfSession)->get();
              foreach($value as $test){
                  $array = explode(",",$test->BookingTime);
                  foreach($allBookingtime as $Bookintimetest){
                      if (in_array($Bookintimetest, $array))
                      {
                          $value_new = CoachBooking::where('im_coach_users_id',$request->BookingCoachId)->where('BookingDate',$DateOfSession)->get();
                          $totalTime = "";
                          foreach($value_new as $value){
                              $totalTime .= $value->BookingTime;
                          }
                            $ti = explode(",",$totalTime);
                            sort( $ti );
                            $clength = count($ti);
                            $totalTime = "";
                            for($x = 0; $x < $clength; $x++) {
                                $totalTime .= $ti[$x].",";
                            }
                            
                            
                            
                            $mes = json_encode(array('status'=>'error' , 'message'=>str_replace(","," , ",$totalTime)));
                            echo $mes;
                            return;
                      }
                    
                  }
              }
              
              $mes = json_encode(array('status'=>'success'));
             echo $mes;
             //echo $totalTime;
             //echo date('h:m:s' ,$StartSession )."  ".date('h:m:s' ,$endSession )." ".$diff;
             
            
        }catch(\Exception $e){
             $mes = json_encode(array('status'=>'error' , 'message'=>' Sorry ! For'.$e->getMessage(),'link'=>url('profile/findcoach/payment')));
            echo $mes;
        }
    }
    
    public function PayPalPaymentGet(){


        

            try{
                /*
                 $apiContext = new \PayPal\Rest\ApiContext(
                        new \PayPal\Auth\OAuthTokenCredential(
                            env("PAYPAL_CLIENT"),     // ClientID
                            env("PAYPAL_SECRET")      // ClientSecret
                            )
                        );
                $apiContext->setConfig(
                      array(
                        
                        'mode' => 'live',
                        
                      )
                );        
                        
                        $paymentId = request('paymentId');
                        $payment = Payment::get($paymentId, $apiContext);
                        
                        $execution = new PaymentExecution();
                        $execution->setPayerId(request('PayerID'));
                        
                        $details = new Details();

                        $details->setSubtotal(30);
                        
                        $transaction = new Transaction();
                        $amount = new Amount();
                        
                        $amount->setCurrency('GBP');
                        $amount->setTotal(30);
                        

                    
                        $transaction->setAmount($amount);
                        $result = $payment->execute($execution, $apiContext);
                        print_r($result);
                        echo "<br><br><br>";
                        $payment = Payment::get($paymentId, $apiContext);
                        print_r($result);
                     if ($result->getState() == 'approved') { 
                         echo "<br><br><br>";
                         echo "OK";
                     }
                
                
                
                */
                
                
            
                
                
                if( 
                    Cookie::has("PAYPAL_BOOKING_COACH_ID") && Cookie::has("PAYPAL_BOOKING_FIND_USER_ID") && Cookie::has("PAYPAL_BOOKING_DATE") && Cookie::has("PAYPAL_BOOKING_TIME") &&
                    Cookie::has("PAYPAL_BOOKING_TOTALHOUR") && Cookie::has("PAYPAL_BOOKING_TOTALCOST") && Cookie::has("PAYPAL_BOOKING_COACHPERHOURPRICE") && Cookie::has("PAYPAL_BOOKING_LOCATION")
                ){
                
                      $apiContext = new \PayPal\Rest\ApiContext(
                        new \PayPal\Auth\OAuthTokenCredential(
                            env("PAYPAL_CLIENT"),     // ClientID
                            env("PAYPAL_SECRET")      // ClientSecret
                            )
                        );
                        $apiContext->setConfig(array('mode' => env('PAYPAL_ENV'),));
                        $paymentId = request('paymentId');
                        $payment = Payment::get($paymentId, $apiContext);
                        
                        $execution = new PaymentExecution();
                        $execution->setPayerId(request('PayerID'));
                        
                        $transaction = new Transaction();
                        $amount = new Amount();
                        
                        $amount->setCurrency('GBP');
                        $amount->setTotal(\Cookie::get("PAYPAL_BOOKING_TOTALCOST"));
                    
                        $transaction->setAmount($amount);
                        $result = $payment->execute($execution, $apiContext);
                        //print_r($result);
                        //echo "<br><br><br>";
                        $payment = Payment::get($paymentId, $apiContext);
                        //print_r($result);
                        
                        
                        $coachbooking = new CoachBooking;
                        $coachbooking->im_coach_users_id = \Cookie::get("PAYPAL_BOOKING_COACH_ID");
                        $coachbooking->find_coach_users_id = \Cookie::get("PAYPAL_BOOKING_FIND_USER_ID");
                        $coachbooking->BookingDate = \Cookie::get("PAYPAL_BOOKING_DATE");
                        $coachbooking->BookingTime = \Cookie::get("PAYPAL_BOOKING_TIME");
                        $coachbooking->BookingTotalHours = \Cookie::get("PAYPAL_BOOKING_TOTALHOUR");
                        $coachbooking->ParhourCoachAmount = \Cookie::get("PAYPAL_BOOKING_COACHPERHOURPRICE");
                        $coachbooking->totalAmount = \Cookie::get("PAYPAL_BOOKING_TOTALCOST");
                        $coachbooking->Location = \Cookie::get("PAYPAL_BOOKING_LOCATION");
                        
                        $coachbooking->PaymentWayFindCoach = 1;
                        $coachbooking->PaymentFindCoachAllInfo = $result."<RESULT_PAYMRNT_ALL_INFO>".$payment;
                        $coachbooking->isReviewMessageSend = "0";
                        $coachbooking->isSessionComplete = "0";
                        $coachbooking->save();
                       
                       
                       
                        cookie('PAYPAL_BOOKING_COACH_ID', '', -1);
                        cookie('PAYPAL_BOOKING_FIND_USER_ID', '', -1);
                        cookie('PAYPAL_BOOKING_DATE', '', -1);
                        cookie('PAYPAL_BOOKING_TIME', '', -1);
                        
                        cookie('PAYPAL_BOOKING_TOTALHOUR', '', -1);
                        cookie('PAYPAL_BOOKING_TOTALCOST', '', -1);
                        cookie('PAYPAL_BOOKING_COACHPERHOURPRICE', '', -1);
                        cookie('PAYPAL_BOOKING_LOCATION', '', -1);
                       
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_COACH_ID");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_FIND_USER_ID");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_DATE");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_TIME");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_TOTALHOUR");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_TOTALCOST");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_COACHPERHOURPRICE");
                        $cookie = \Cookie::forget("PAYPAL_BOOKING_LOCATION");
                        
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_COACH_ID'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_FIND_USER_ID'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_DATE'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_TIME'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_TOTALHOUR'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_TOTALCOST'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_COACHPERHOURPRICE'));
                        \Cookie::queue(Cookie::forget('PAYPAL_BOOKING_LOCATION'));
                        
                        
                        return Redirect::to('profile/findcoach/bookingcoachlist')->withCookie($cookie);
                        
                        
                        
                        
                         
                }
                else{
                    echo "Session Not Find";
                   // session()->forget('PAYPAL_BOOKING_PAYPAL_PAYMENT_CREATE');
                //    session()->forget('PAYPAL_BOOKING_REQUEST_VALUE');
                  //  session()->forget('PAYPAL_BOOKING_FIND_USER_ID');
                //    session()->forget('PAYPAL_BOOKING_COACH_ID');
                }
                
               
            }catch(\Exception $e){
                echo $e;
            }
    }
    
    
    
}
