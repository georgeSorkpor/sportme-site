<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\FindCoachUser;
use App\Mail\SendMail;

use Mail;

class FindCoachUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }
    
    
    
    
    public function registation(){
        try{
            return view('findcoach.reg');
        }catch(\Exception $e){
            echo $e ;
        }
    }
    public function login(){
        try{
            return view('findcoach.login');
        }catch(\Exception $e){
            echo $e ;
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function EmailVarif($id)
    {
        $value = FindCoachUser::where('remember_token', $id)->first();
        if($value){
            if($value->isverifyemailstatus == 0 && $value->email_verified_at == null && $value->remember_token !== "0" ){
                 $value = FindCoachUser::where('remember_token', $id)->update([
                'isverifyemailstatus' => 1 ,
                "email_verified_at"=> date("Y-m-d h:m:s",strtotime("now")),
                "remember_token"=> "0"
                ]);
                
                if($value){
                    
                    echo "Email verified <a href=\"".url('findcoach/log')."\"> Click Here To Login</a>";
                }
                else{
                     echo "Email Not Verified Some Error , Please Contact Us...";
                }
            }
            else{
                echo "Alrady verified Email";
            }
           
        }
        else{
            echo "Token is Wrong!! , Check Email Or Please Contact Us..";
        }
       
    }
    
    
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
         try {
           $count = FindCoachUser::count();
           $UserFindCoach = new FindCoachUser;
           $UserFindCoach->usertype = $request->user_type;
           $UserFindCoach->firstname = $request->first_name;
           $UserFindCoach->lastname = $request->last_name;
           $UserFindCoach->email = $request->email;
           $UserFindCoach->password = md5($request->password);
           $UserFindCoach->phonenumber = $request->phone;
           $UserFindCoach->birthday = $request->birthday;
           
           if($request->FindCoachuserInterest !== "0"){
              $UserFindCoach->sport_interest = $request->FindCoachuserInterest;
           }
           else{
                $UserFindCoach->sport_interest = "0";
           }
           
           $UserFindCoach->location = $request->LocationSelect;
           $UserFindCoach->isverifyemailstatus = "0";
           $val = "sportme_".str_random(21)."_".($count+1)."_1";
           $UserFindCoach->remember_token = $val;
           
           $UserFindCoach->save();
           
           Mail::send(new SendMail($val));
           
           
           
           $arr = array("status" => "1" ,"link" => url('findcoach/log')); 
           return json_encode($arr);
        }catch(Exception $e){
             $arr = array("status" => "0"); 
             return json_encode($arr);
            echo "Error In exception "; 
        }
       
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
