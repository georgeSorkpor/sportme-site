<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoachBooking extends Model
{
    public function Coach()
    {
        return $this->hasMany(ImCoachUser::class , 'id','im_coach_users_id');
    }
    public function FindCoach()
    {
        return $this->hasMany(FindCoachUser::class , 'id', 'find_coach_users_id');
    }
}
