$(document).ready(function () {

	var $slider = $('.slider-top');

	if ($slider.length) {
	  var currentSlide;
	  var slidesCount;
	  var sliderCounter = document.createElement('div');
	  sliderCounter.classList.add('slider__counter');
	  
	  var updateSliderCounter = function(slick, currentIndex) {
	    currentSlide = slick.slickCurrentSlide() + 1;
	    slidesCount = slick.slideCount;
	    $(sliderCounter).text(currentSlide + ' of ' +slidesCount)
	  };

	  $slider.on('init', function(event, slick) {
	    $('.slider-nav').append(sliderCounter);
	    updateSliderCounter(slick);
	  });

	  $slider.on('afterChange', function(event, slick, currentSlide) {
	    updateSliderCounter(slick, currentSlide);
	  });

	  $slider.slick({
		dots: false,
		arrows: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		appendArrows: $('.slider-nav'),
		responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
    ]
	});
	}

	var $slider2 = $('.reviews');

	if ($slider2.length) {
	  var currentSlide;
	  var slidesCount;
	  var sliderCounter = document.createElement('div');
	  sliderCounter.classList.add('slider__counter');
	  
	  var updateSliderCounter = function(slick, currentIndex) {
	    currentSlide = slick.slickCurrentSlide() + 1;
	    slidesCount = slick.slideCount;
	    $(sliderCounter).text(currentSlide + ' of ' +slidesCount)
	  };

	  $slider2.on('init', function(event, slick) {
	    $('.reviews-nav').append(sliderCounter);
	    updateSliderCounter(slick);
	  });

	  $slider2.on('afterChange', function(event, slick, currentSlide) {
	    updateSliderCounter(slick, currentSlide);
	  });

	  $slider2.slick({
		dots: false,
		arrows: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		appendArrows: $('.reviews-nav'),
		responsive: [
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 1,
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
    ]
	});
	}
		
	// register
	$('#registration').validate({
		submitHandler: function (form){
			console.log('clicked');
			
			first_name = $('#first_name').val();
			last_name = $('#last_name').val();
			email = $('#email').val();
			password = $('#password').val();
			password_confirm = $('#password_confirm').val();
			birthday = $('#birthday').val();
			phone = $('#phone').val();
            $.post('http://sportme.teamalfy.com/admin/public/api/step_register', {
            	'_token': '{{csrf_token()}}',
            	'user_type' : 1,
            	// 'step' : step,
            	'first_name' : first_name,
            	'last_name' : last_name,
            	'email' : email,
            	'password' : password,
            	'birthday' : birthday,
            	'phone' : phone,
            	},
            	function(response,status){ // Required Callback Function
            		alert("*----Received Data----*nnResponse : " + response+"nnStatus : " + status);//"response" receives - whatever written in echo of above PHP script.
            		$("#form")[0].reset();
            	});
         },
	    
	    rules: {
	        first_name: {
	            required: true,
	            minlength: 2
	        },
	        last_name: {
	            required: true,
	            minlength: 2
	        },
	        email: {
	            required: true,
	            email: true
	        },
	        password: {
	            required: true,
	            minlength: 5
	        },
	        password_confirm: {
	            required: true,
	            minlength: 5,
	            equalTo: "#password"
	        },
	        birthday: {
	            required: true,
	            minlength: 2
	        },
	        phone: {
	            required: true,
	            minlength: 9
	        }
	    },
	    messages: {
	        phone: {
	            minlength: "Your phone number must consist of at least 9 characters"
	        }
	    }
	});
	
	$('#registration_coach').validate({
		submitHandler: function (form){
			console.log('clicked');
			
			first_name_coach = $('#first_name_coach').val();
			second_name_coach = $('#second_name_coach').val();
			email_coach = $('#email_coach').val();
			password_coach = $('#password_coach').val();
			password_confirm_coach = $('#password_confirm_coach').val();
			birthday_coach = $('#birthday_coach').val();
			phone_number_coach = $('#phone_number_coach').val();
			code = $('#code').val();
			account_name = $('#account_name').val();
			account_number = $('#account_number').val();
			sort_code = $('#sort_code').val();
			card_type = $('#card_type').val();
			card_number = $('#card_number').val();
			name_on_card = $('#name_on_card').val();
			billing_address = $('#billing_address').val();
			expire_date = $('#expire_date').val();
			cvc = $('#cvc').val();
			coach_in_list = $('#coach_in_list').val();
			photo = $('#photo').val();

            $.post('http://sportme.teamalfy.com/admin/public/api/step_register', {
            	'user_type' : 2,
            	'first_name_coach' : first_name_coach,
            	'second_name_coach' : second_name_coach,
            	'email_coach' : email_coach,
            	'password_coach' : password_coach,
            	'birthday_coach' : birthday_coach,
            	'phone_number_coach' : phone_number_coach,
            	'code' : code,
            	'account_name' : account_name,
            	'account_number' : account_number,
            	'sort_code' : sort_code,
            	'card_type' : card_type,
            	'card_number' : card_number,
            	'name_on_card' : name_on_card,
            	'billing_address' : billing_address,
            	'expire_date' : expire_date,
            	'cvc' : cvc,
            	'coach_in_list' : coach_in_list,
            	'photo' : photo,
            	},
            	function(response,status){ // Required Callback Function
            		alert("*----Received Data----*nnResponse : " + response+"nnStatus : " + status);//"response" receives - whatever written in echo of above PHP script.
            		$("#form")[0].reset();
            	});
         },
	    
	    rules: {
	        first_name: {
	            required: true,
	            minlength: 2
	        },
	        last_name: {
	            required: true,
	            minlength: 2
	        },
	        email: {
	            required: true,
	            email: true
	        },
	        password: {
	            required: true,
	            minlength: 5
	        },
	        password_confirm: {
	            required: true,
	            minlength: 5,
	            equalTo: "#password"
	        },
	        birthday: {
	            required: true,
	            minlength: 2
	        },
	        phone: {
	            required: true,
	            minlength: 9
	        }
	    },
	    messages: {
	        phone: {
	            minlength: "Your phone number must consist of at least 9 characters"
	        }
	    }
	});

	
});


