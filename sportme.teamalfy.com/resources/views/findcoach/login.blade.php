<!DOCTYPE html>
<!-- saved from url=(0034)http://sportme.teamalfy.com/login/ -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Google fonts -->
	<link href="{{ url('res_login/css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
	<title>Login – Sportme</title>
<meta name="robots" content="noindex,follow">
<link rel="dns-prefetch" href="http://ajax.googleapis.com/">
<link rel="dns-prefetch" href="http://s.w.org/">
<link rel="alternate" type="application/rss+xml" title="Sportme » Feed" href="http://sportme.teamalfy.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Sportme » Comments Feed" href="http://sportme.teamalfy.com/comments/feed/">
<script src="{{ url('res_login/wp-emoji-release.min.js') }}" type="text/javascript" defer=""></script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="wp-block-library-css" href="{{ url('res_login/style.min.css') }}" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css" href="{{ url('res_login/styles.css') }}" type="text/css" media="all">
<link rel="stylesheet" id="slick-css-css" href="{{ url('res_login/slick.css') }}" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-css" href="{{ url('res_login/bootstrap.min.css') }}" type="text/css" media="all">
<link rel="stylesheet" id="sportme-style-css" href="{{ url('res_login/style.css') }}" type="text/css" media="all">
<script type="text/javascript" src="{{ url('res_login/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/custom.js') }}"></script>
<link rel="https://api.w.org/" href="http://sportme.teamalfy.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://sportme.teamalfy.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://sportme.teamalfy.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 5.0.3">
<link rel="canonical" href="http://sportme.teamalfy.com/login/">
<link rel="shortlink" href="http://sportme.teamalfy.com/?p=94">
<link rel="alternate" type="application/json+oembed" href="http://sportme.teamalfy.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fsportme.teamalfy.com%2Flogin%2F">
<link rel="alternate" type="text/xml+oembed" href="http://sportme.teamalfy.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fsportme.teamalfy.com%2Flogin%2F&amp;format=xml">
</head>

<body class="page-template page-template-login-page page-template-login-page-php page page-id-94" data-__gyazo-extension-added-paste-support="true">
	<header style="background:  url(&#39;&#39;) no-repeat center top ; ">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-md-3 order-md-1 order-2">
					<ul class="social">
						<li>
							<a href="http://sportme.teamalfy.com/" class="fb"></a>
						</li>
						<li>
							<a href="http://sportme.teamalfy.com/" class="tw"></a>
						</li>
						<li>
							<a href="http://sportme.teamalfy.com/" class="inst"></a>
						</li>
						<li>
							<a href="http://sportme.teamalfy.com/" class="link"></a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 order-md-2 order-1">
					<div class="middle">
					    <div id="bs4navbar" class="ml-auto"><ul id="primary-menu" class=""><li id="menu-item-75" class="logo menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-75"><a href="{{ url('/') }}">Home</a></li>
<li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="http://sportme/about-us/">About us</a></li>
<li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="http://sportme/careers/">Careers</a></li>
<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="http://sportme/contact-us/">Contact us</a></li>
</ul></div>					</div>
				</div>
				<div class="col-md-3 order-3">
					<div class="info">
						<a href="tel:+42 2355 4213" class="phone">+42 2355 4213</a>
						<a href="mailto:support@teamalfy.com.com" class="email">support@teamalfy.com.com</a>
					</div>
				</div>
			</div>
							<h2>Login</h2>
				<p class="desc"></p>
					</div>
	</header>
<div id="primary" class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-8">
          @if ($errors->any())
                <div class="alert alert-danger" >
                    <center>
                         @foreach ($errors->all() as $error)
                            {{ $error }}
                         @endforeach
                    </center>
                       
                </div>
            @endif
        <form action="{{ url('findcoach/logInTry') }}" method="POST" class="login-form">
		  <h2>SPORTME LOGING FINDING YOUR COACH</h2>
		 {{@csrf_field()}}
		  <input type="hidden" class="form-control" name="user_type" value="1">
          <div class="form-group">
              <input type="email" class="form-control" id="username" aria-describedby="emailHelp" name="email" placeholder="Email" required>
          </div>
          <div class="form-group">
              <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
          </div>
          <div class="center">
            <button type="submit">Login</button>
            <a href="{{url('findcoach/forgotpass')}}" class="forgot">Forgot your password?</a>
          </div>
          
          
        </form>
      </div>
    </div>
  </div>
</div><!-- #primary -->

<footer>
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-3">
				<ul class="social">
					<li>
						<a href="http://sportme.teamalfy.com/" class="fb"></a>
					</li>
					<li>
						<a href="http://sportme.teamalfy.com/" class="tw"></a>
					</li>
					<li>
						<a href="http://sportme.teamalfy.com/" class="inst"></a>
					</li>
					<li>
						<a href="http://sportme.teamalfy.com/" class="link"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-6">
				<span class="copy">Copyright (c) All Rights Reserved - <b>SportMe</b></span>
			</div>
			<div class="col-md-3">
				<div class="info">
					<a href="tel:+42 2355 4213" class="phone">+42 2355 4213</a>
					<a href="mailto:support@teamalfy.com.com" class="email">support@teamalfy.com.com</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/sportme.teamalfy.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type="text/javascript" src="{{ url('res_login/scripts.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/navigation.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/skip-link-focus-fix.js') }}"></script>
<script type="text/javascript" src="{{ url('res_login/wp-embed.min.js') }}"></script>



</body></html>