<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Homepage test2</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="{{ url('path/to/image.jpg') }}">

	<link rel="shortcut icon" href="{{ url('img/favicon/favicon.ico') }}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{ url('img/favicon/apple-touch-icon.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ url('img/favicon/apple-touch-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ url('img/favicon/apple-touch-icon-114x114.png') }}">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,700i" rel="stylesheet">
	<!-- Header CSS (First Sections of Website: paste after release from header.min.css here) -->
	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
    
	<link rel="stylesheet" href="{{ url('css/fonts.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/header.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/main.min.css') }}">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
	 

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>

<body class="page-template-front-page">
	<header>
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-md-3 order-md-1 order-2">
					<ul class="social">
						<li>
							<a href="#" class="fb"></a>
						</li>
						<li>
							<a href="#" class="tw"></a>
						</li>
						<li>
							<a href="#" class="inst"></a>
						</li>
						<li>
							<a href="#" class="link"></a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 order-md-2 order-1">
					<div class="middle">
						<ul id="primary-menu" class="">
							<li id="menu-item-75" class="logo menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-75"><a href="{{ url('/') }}">Home</a></li>
							<li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="{{ url('about') }}">About us</a></li>
							<li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="{{ url('careers') }}">Careers</a></li>
							<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="{{ url('contactus') }}">Contact us</a></li>
						</ul>					
					</div>
				</div>
				<div class="col-md-3 order-3">
					<div class="info">
						<a href="tel:4223554213" class="phone">+42 2355 4213</a>
						<a href="mailto:support@sportme.com" class="email">support@sportme.com</a>
					</div>
				</div>
			</div>
			<div class="register-blocks">
				<div class="row">
					<h1>Find a coach near you in your chosen<span>sports for world-class training</span></h1>
					<div class="col-md-6">
						<div class="register-block">
							<span class="title">Find your coach</span>
							<a href="#" class="btn" data-toggle="modal" id="FindCotchButton" data-target="#registerModal">Get started</a>
							<span class="inf">Already a member? <a href="{{ url('findcoach/log') }}">Login</a></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="register-block second">
							<span class="title">I'm a coach</span>
							<a href="#" class="btn" data-toggle="modal" data-target="#registerModal2" id="abc">Get started</a>
							<span class="inf">Already a member? <a href="{{ url('imcoach/log') }}">Login</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
<!-- modal register -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          
           
	        <div id = "div1FindCatch">

	     
                <script>
                $(document).ready(function(){
                   /* $("#FindCotchButton").click(function(){
                        $("#div1FindCatch").load("{{url('findcoach/reg')}}");
                    });
                    */
                    $("#div1FindCatch").load("{{url('findcoach/reg')}}");

                  
                });
                </script>
                
               
	        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal register coach -->
<div class="modal fade show" id="registerModal2" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        
      <div class="modal-body">
          
           
      	<!-- <form action="http://sportme.teamalfy.com/admin/public/api/step_register" id="registration" method="post"> -->
      	<form action="{{ url('imcoach/regtry/1') }}" id="registration_Form_ImCoach" method="post">
      	    
      	     
              
	        <div class="nav nav-tabs" id="nav-tab" role="tablist">
	          <a class="nav-item nav-link active" id="nav-step1-tab" data-toggle="tab" href="#nav-step1" role="tab" aria-controls="nav-step1" aria-selected="true">1 Step</a>
	          <a class="nav-item nav-link" id="nav-step2-tab" data-toggle="tab" href="#nav-step2" role="tab" aria-controls="nav-step2" aria-selected="false">2 Step</a>
	          <a class="nav-item nav-link" id="nav-step3-tab" data-toggle="tab" href="#nav-step3" role="tab" aria-controls="nav-step3" aria-selected="false">3 Step</a>
	          <a class="nav-item nav-link" id="nav-step4-tab" data-toggle="tab" href="#nav-step4" role="tab" aria-controls="nav-step4" aria-selected="false">4 Step</a>
	        </div>
	        <div class="tab-content" id="nav-tabContent">
	          <div class="tab-pane first fade show active" id="nav-step1" role="tabpanel" aria-labelledby="nav-step1-tab">
	             
	          	<div class="row">
	          	     
		        	<div class="col-md-8 left">
		        	    @if ($errors->any())
                            <div class="alert alert-danger" >
                                <center>
                                     @foreach ($errors->all() as $error)
                                        {{ $error }}
                                     @endforeach
                                </center>
                                   
                            </div>
                        @endif
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="first_name_coach">First Name</label>
		        				    <input type="text" class="form-control" id="first_name_coach" name="first_name_coach" value="{{old('first_name_coach')}}" required>
		        				</div>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="second_name_coach">Second Name</label>
		        				    <input type="text" class="form-control" id="second_name_coach" name="second_name_coach" value = "{{old('second_name_coach')}}" required>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-12">
		        				<div class="form-group">
		        				    <label for="email_coach">Email</label>
		        				    <input type="email" class="form-control" id="email_coach" name="email_coach" value = "{{old('email_coach')}}" required>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="password_coach">Choose a password</label>
		        				    <input type="password" class="form-control" id="password_coach" name="password_coach" required>
		        				</div>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="password_confirm_coach">Re-confirm password</label>
		        				    <input type="password" class="form-control" id="password_confirm_coach" name="password_confirm_coach" required>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="birthday_coach">Birthday</label>
		        				    <input type="numeber" class="form-control input-date-coach" id="birthday_coach" value = "{{old('birthday_coach')}}" placeholder="DD/MM/YYYY" name="birthday_coach" required>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="phone_number_coach">Phone number</label>
		        				    <input type="tel" class="form-control input-phone-coach" id="phone_number_coach" value = "{{old('phone_number_coach')}}" name="phone_number_coach" required>
		        				</div>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-md-4 register coach">
		        	    <a href='{{url('/')}}'>
		        		    <button type="button" class="close" ></button>
		        		</a>
		        		{{csrf_field()}}
		        		<button href="#" class="button" id = "UserFindCoatchSubmitButton">Next</button>
		        	</div>
		        </div>
	         
		        	
		        </div>
	          </div>
	        </div>
        </form>
       
      </div>
    </div>
  </div>
</div>



<script>
$(document).ready(function(){
  // Show the Modal on load
  $("#registerModal2").modal("show");
    

});


</script>
                
                
<section class="section-1">
  <div class="container-fluid">
    <div class="poster">
      <img src="{{ url('img/home/section_1-1.png') }}" alt="">
    </div>
  </div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-11">
        <h2>GET STARTED</h2>
        <p>At SportMe, we connect everyday people with sports coaches across the capital. Whether you're looking to learn a new skill or train with top professionals, we have coaches at hand to help you achieve your fitness goals. Our coaches offer a variety of disciplines and charge a transparent hourly fee. Not comfortable with a one-on-one session? Not a problem! You can even choose to join a class to begin with. So why not get started and Find Your Coach (hyperlink Find our Coach to sign up today.</p>
      </div>
    </div>
  </div>
</section>
<section class="section-2">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 order-md-1 order-3"><div class="slider-nav"></div></div>
      <div class="col-md-4 order-md-2 order-1"><h2>Top rated</h2></div>
      <div class="col-md-4 order-md-3 order-2"><a href="#" class="view_all">View all</a></div>
    </div>
  </div>
  <div class="slider-top">
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top1.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Dan Courke</span>
            <span class="position">Weight Lifter</span>
            <span class="price">35$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
           </div>
            <span class="rate-count">25 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top2.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Jilea Hemming</span>
            <span class="position">Martial Artist</span>
            <span class="price">29$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star"></span>
           </div>
            <span class="rate-count">20 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top3.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">David Jerome</span>
            <span class="position">Yoga Master</span>
            <span class="price">22$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star half"></span>
             <span class="star"></span>
           </div>
            <span class="rate-count">25 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top4.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Christine</span>
            <span class="position">Weight Lifter</span>
            <span class="price">40$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
           </div>
            <span class="rate-count">15 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top3.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Mike</span>
            <span class="position">Yoga Master</span>
            <span class="price">23$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
           </div>
            <span class="rate-count">27 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top4.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Christine</span>
            <span class="position">Weight Lifter</span>
            <span class="price">34$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
           </div>
            <span class="rate-count">29 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
    <div class="item">
      <div class="inner">
        <div class="top">
          <div class="left">
            <div class="image">
              <img src="{{ url('img/home/top1.png') }}" alt="">
            </div>
          </div>
          <div class="right">
            <span class="name">Joel</span>
            <span class="position">Martial Artist</span>
            <span class="price">30$/hr</span>
            <div class="rate">
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
             <span class="star fill"></span>
           </div>
            <span class="rate-count">26 Reviews</span>
          </div>
        </div>
        <a href="#" class="view">View profile</a>
      </div>
    </div>
  </div>
</section>
<section class="section-3">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h2>how it works</h2>
        <p>Simply choose to Find Your Coach and browse our network of professional coaches within your location. Once you chose a coach, select from their availability and venue options, and you're all set to go. It really is as easy as that!</p>
      </div>
    </div>
  </div>
  <div class="steps">
    <div class="item">
      <span class="number">1</span>
      <span class="title">tell us your location</span>
      <p>Browse coaches in selected locations</p>
    </div>
    <div class="item">
      <span class="number">2</span>
      <span class="title">choose YOUR SPORTS</span>
      <p>Whether you're looking to take up swimming or perfect that golf swing (or tennis!) select which sports you would like to practice.</p>
    </div>
    <div class="item">
      <span class="number">3</span>
      <span class="title">BOOK A SESSION</span>
      <p>Once you're happy with the coach, their availability and venue choice, simply go ahead and book the slot. All that's left is to enjoy your session!</p>
    </div>
  </div>
</section>
<section class="section-4">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8">
        <img src="{{ url('img/home/rocket.png') }}" alt="">
        <h2>Our clients</h2>
        <p>Review what our clients say</p>
      </div>
      <div class="col-12">
        <div class="reviews">

              <div class="item">
                <div class="inner">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="image">
                        <img src="{{ url('img/home/review.png') }}" alt="">
                      </div>
                    </div>
                    <div class="col-12 col-md-9">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                      <span class="name">Dan Courke</span>
                      <span class="pos">Weight Lifter</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="inner">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="image">
                        <img src="{{ url('img/home/review.png') }}" alt="">
                      </div>
                    </div>
                    <div class="col-12 col-md-9">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                      <span class="name">Dan Courke</span>
                      <span class="pos">Weight Lifter</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="inner">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="image">
                        <img src="{{ url('img/home/review.png') }}" alt="">
                      </div>
                    </div>
                    <div class="col-12 col-md-9">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                      <span class="name">Dan Courke</span>
                      <span class="pos">Weight Lifter</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="inner">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="image">
                        <img src="{{ url('img/home/review.png') }}" alt="">
                      </div>
                    </div>
                    <div class="col-12 col-md-9">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                      <span class="name">Dan Courke</span>
                      <span class="pos">Weight Lifter</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="inner">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="image">
                        <img src="{{ url('img/home/review.png') }}" alt="">
                      </div>
                    </div>
                    <div class="col-12 col-md-9">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</p>
                      <span class="name">Dan Courke</span>
                      <span class="pos">Weight Lifter</span>
                    </div>
                  </div>
                </div>
              </div>

        </div>
        <div class="reviews-nav"></div>
      </div>
    </div>
  </div>
</section>

<footer>
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-3">
				<ul class="social">
					<li>
						<a href="#" class="fb"></a>
					</li>
					<li>
						<a href="#" class="tw"></a>
					</li>
					<li>
						<a href="#" class="inst"></a>
					</li>
					<li>
						<a href="#" class="link"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-6">
				<span class="copy">Copyright (c) All Rights Reserved - <b>SportMe</b></span>
			</div>
			<div class="col-md-3">
				<div class="info">
					<a href="tel:4223554213" class="phone">+42 2355 4213</a>
					<a href="mailto:support@sportme.com" class="email">support@sportme.com</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<script>
    
 $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $("#registration_Form_ImCoach").validate({
                 rules : {
                        password_coach : {
                            minlength : 6
                        },
                        password_confirm_coach : {
                            minlength : 6,
                            equalTo : "#password_coach"
                        },
                 },
                submitHandler : function(){
                    $('#UserFindCoatchSubmitButton').attr("disabled", "disabled");
                   $('#UserFindCoatchSubmitButton').html("<i class=\"fa fa-spinner fa-spin\"></i>");
                    
                   
                    
                    
                    var postValue = jQuery("#registration_Form_FindCoach").serialize()+"&FindCoachuserInterest="+intre;
                    console.log(postValue);
                    
                    
                   
                    
                    
                    
                }
            });
            
        });
    
</script>
	<div class="hidden"></div>
   
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<!-- Load Scripts Start -->
	<script>var scr = {"scripts":[
		{"src" : "{{ url('js/libs.js') }}", "async" : false},
		{"src" : "{{ url('js/jquery-ui.min.js') }}", "async" : false},
		{"src" : "{{ url('js/home/cleave.min.js') }}", "async" : false},
		{"src" : "{{ url('js/home/cleave-phone.gb.js') }}", "async" : false},
		{"src" : "{{ url('js/home/phone-type-formatter.gb.js') }}", "async" : false},
		{"src" : "{{ url('js/home/jquery.validate.min.js') }}", "async" : false},
		{"src" : "{{ url('js/common.js') }}", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>
	<!-- Load Scripts End -->

</body>
</html>
