<html lang="en-US"><head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
	<title>Contact us – Sportme</title>
<meta name="robots" content="noindex,follow">
<link rel="dns-prefetch" href="//ajax.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="alternate" type="application/rss+xml" title="Sportme » Feed" href="http://sportmedash.teamalfy.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Sportme » Comments Feed" href="http://sportmedash.teamalfy.com/comments/feed/">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/sportmedash.teamalfy.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><script src="http://sportmedash.teamalfy.com/wp-includes/js/wp-emoji-release.min.js?ver=5.0.3" type="text/javascript" defer=""></script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="dashicons-css" href="https://sportmedash.teamalfy.com/wp-includes/css/dashicons.min.css?ver=5.0.3" type="text/css" media="all">
<link rel="stylesheet" id="admin-bar-css" href="https://sportmedash.teamalfy.com/wp-includes/css/admin-bar.min.css?ver=5.0.3" type="text/css" media="all">
<link rel="stylesheet" id="wp-block-library-css" href="http://sportmedash.teamalfy.com/wp-includes/css/dist/block-library/style.min.css?ver=5.0.3" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css" href="https://sportmedash.teamalfy.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1" type="text/css" media="all">
<link rel="stylesheet" id="slick-css-css" href="https://sportmedash.teamalfy.com/wp-content/themes/sportme/sass/slick.css?ver=5.0.3" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-css" href="https://sportmedash.teamalfy.com/wp-content/themes/sportme/sass/bootstrap.min.css?ver=5.0.3" type="text/css" media="all">
<link rel="stylesheet" id="sportme-style-css" href="https://sportmedash.teamalfy.com/wp-content/themes/sportme/style.css?ver=5.0.3" type="text/css" media="all">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/custom.js?ver=1"></script>
<link rel="https://api.w.org/" href="https://sportmedash.teamalfy.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://sportmedash.teamalfy.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://sportmedash.teamalfy.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 5.0.3">
<link rel="canonical" href="https://sportmedash.teamalfy.com/contact-us/">
<link rel="shortlink" href="https://sportmedash.teamalfy.com/?p=69">
<link rel="alternate" type="application/json+oembed" href="https://sportmedash.teamalfy.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fsportmedash.teamalfy.com%2Fcontact-us%2F">
<link rel="alternate" type="text/xml+oembed" href="https://sportmedash.teamalfy.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fsportmedash.teamalfy.com%2Fcontact-us%2F&amp;format=xml">
<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
</head>

<body class="page-template-default page page-id-69 logged-in admin-bar  customize-support">
	<header style="background: #020233 url('https://sportmedash.teamalfy.com/wp-content/uploads/2019/01/contact.png') no-repeat center top !important; ">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-md-3 order-md-1 order-2">
					<ul class="social">
						<li>
							<a href="https://sportme.teamalfy.com/" class="fb"></a>
						</li>
						<li>
							<a href="https://sportme.teamalfy.com/" class="tw"></a>
						</li>
						<li>
							<a href="https://sportme.teamalfy.com/" class="inst"></a>
						</li>
						<li>
							<a href="https://sportme.teamalfy.com/" class="link"></a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 order-md-2 order-1">
					<div class="middle">
					    <div id="bs4navbar" class="ml-auto"><ul id="primary-menu" class=""><li id="menu-item-75" class="logo menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-75"><a href="https://sportmedash.teamalfy.com/">Home</a></li>
<li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="https://sportmedash.teamalfy.com/about-us/">About us</a></li>
<li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="https://sportmedash.teamalfy.com/careers/">Careers</a></li>
<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-69 current_page_item menu-item-73"><a href="https://sportmedash.teamalfy.com/contact-us/">Contact us</a></li>
</ul></div>					</div>
				</div>
				<div class="col-md-3 order-3">
					<div class="info">
						<a href="tel:+44 7479 546055" class="phone">+44 7479 546055</a>
						<a href="mailto:support@sportme.com" class="email">support@sportme.com</a>
					</div>
				</div>
			</div>
							<h2>Contact us</h2>
				<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nec interdum tortor. Vivamus et nisi vel arcu aliquam sagittis efficitur in arcu. In bibendum, nunc a condimentum suscipit, magna justo suscipit arcu, in ultrices nisl lectus sit amet ipsum.</p>
					</div>
	</header>
<!-- modal register -->
<div class="modal fade show" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<form action="https://sportme.teamalfy.com/admin/public/api/step_register" id="registration" method="post" novalidate="novalidate">
	        <div class="row">
	            <input type="hidden" name="user_type" value="1">
	        	<div class="col-md-8 left">
	        		<h2>Register with sportme</h2>
	        		<button type="button" class="close d-block d-md-none" data-dismiss="modal" aria-label="Close">
	        		</button>
	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="first_name">First Name</label>
	        				    <input type="text" class="form-control" id="first_name" name="first_name" required="">
	        				</div>
	        			</div>
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="last_name">Last Name</label>
	        				    <input type="text" class="form-control" id="last_name" name="last_name" required="">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-md-12">
	        				<div class="form-group">
	        				    <label for="email">Email</label>
	        				    <input type="email" class="form-control" id="email" name="email" required="">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="password">Password</label>
	        				    <input type="password" class="form-control" id="password" name="password" required="">
	        				</div>
	        			</div>
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="password_confirm">Confirm password</label>
	        				    <input type="password" class="form-control" id="password_confirm" name="password_confirm" required="">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="birthday">Date of Birth</label>
	        				    <input type="text" class="form-control input-date" id="birthday" placeholder="DD/MM/YYYY" name="birthday" required="">
	        				</div>
	        			</div>
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="phone">Phone Number</label>
	        				    <input type="tel" class="form-control input-phone" id="phone" name="phone" required="">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="interest">Sports of Interest</label>
	        				    <div class="dropdown">
    		        	    	  <button class="dropdown-toggle custom-select" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    		        	    	   Choose one or more
    		        	    	  </button>
    		        	    	  <div class="dropdown-menu" aria-labelledby="dropdownMenu">
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radio" type="radio" name="radio">
    	        	    	  			<label for="radio">Bodybuilding</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radio-2" type="radio" name="radio2">
    	        	    	  			<label for="radio-2">Aerobic</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radio-3" type="radio" name="radio3">
    	        	    	  			<label for="radio-3">Yoga</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radio-4" type="radio" name="radio4">
    	        	    	  			<label for="radio-4">Flexibility</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radio-5" type="radio" name="radio5">
    	        	    	  			<label for="radio-5">All</label>
    		        	    	  	</div>
    		        	    	  </div>
    		        	    	  <input type="hidden" name="sports_of_interest" id="sports_of_interest">
    		        	    	</div>
	        				</div>
	        			</div>
	        			<div class="col-md-6">
	        				<div class="form-group">
	        				    <label for="birthday">Location</label>
	        				    <div class="dropdown">
    		        	    	  <button class="dropdown-toggle custom-select" type="button" id="dropdownMenuCity" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    		        	    	   Choose one or more
    		        	    	  </button>
    		        	    	  <div class="dropdown-menu" aria-labelledby="dropdownMenuCity">
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radioCity" type="radio" name="radioCity">
    	        	    	  			<label for="radioCity">UK 1</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radioCity-2" type="radio" name="radioCity2">
    	        	    	  			<label for="radioCity-2">UK 2</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radioCity-3" type="radio" name="radioCity3">
    	        	    	  			<label for="radioCity-3">UK 3</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radioCity-4" type="radio" name="radioCity4">
    	        	    	  			<label for="radioCity-4">UK 4</label>
    		        	    	  	</div>
    		        	    	  	<div class="dropdown-divider"></div>
    		        	    	  	<div class="dropdown-item">
    	        	    	  			<input class="radio" id="radioCity-5" type="radio" name="radioCity5">
    	        	    	  			<label for="radioCity-5">All</label>
    		        	    	  	</div>
    		        	    	  </div>
    		        	    	  <input type="hidden" name="location" id="location">
    		        	    	</div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        	<div class="col-md-4 register">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        		</button>
	        		<button class="button">Register</button>
	        	</div>
	        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal register coach -->
<div class="modal fade" id="registerModal2" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<!-- <form action="https://sportme.teamalfy.com/admin/public/api/step_register" id="registration" method="post"> -->
      	<form action="https://sportme.teamalfy.com/admin/public/api/step_register" id="registration_coach" method="post" novalidate="novalidate">
	        <div class="nav nav-tabs" id="nav-tab" role="tablist">
	          <a class="nav-item nav-link active" id="nav-step1-tab" data-toggle="tab" href="#nav-step1" role="tab" aria-controls="nav-step1" aria-selected="true">1 Step</a>
	          <a class="nav-item nav-link" id="nav-step2-tab" data-toggle="tab" href="#nav-step2" role="tab" aria-controls="nav-step2" aria-selected="false">2 Step</a>
	          <a class="nav-item nav-link" id="nav-step3-tab" data-toggle="tab" href="#nav-step3" role="tab" aria-controls="nav-step3" aria-selected="false">3 Step</a>
	          <a class="nav-item nav-link" id="nav-step4-tab" data-toggle="tab" href="#nav-step4" role="tab" aria-controls="nav-step4" aria-selected="false">4 Step</a>
	        </div>
	        <div class="tab-content" id="nav-tabContent">
	          <div class="tab-pane first fade show active" id="nav-step1" role="tabpanel" aria-labelledby="nav-step1-tab">
	          	<div class="row">
		        	<div class="col-md-8 left">
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="first_name_coach">First Name</label>
		        				    <input type="text" class="form-control" id="first_name_coach" name="first_name_coach" required="">
		        				</div>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="second_name_coach">Second Name</label>
		        				    <input type="text" class="form-control" id="second_name_coach" name="second_name_coach" required="">
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-12">
		        				<div class="form-group">
		        				    <label for="email_coach">Email</label>
		        				    <input type="text" class="form-control" id="email_coach" name="email_coach" required="">
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="password_coach">Password</label>
		        				    <input type="password" class="form-control" id="password_coach" name="password_coach" required="">
		        				</div>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="password_confirm_coach">Confirm Password</label>
		        				    <input type="password" class="form-control" id="password_confirm_coach" name="password_confirm_coach" required="">
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="birthday_coach">Date of Birth</label>
		        				    <input type="text" class="form-control input-date-coach" id="birthday_coach" placeholder="DD/MM/YYYY" name="birthday_coach" required="">
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group">
		        				    <label for="phone_number_coach">Phone Number</label>
		        				    <input type="tel" class="form-control input-phone-coach" id="phone_number_coach" name="phone_number_coach" required="">
		        				</div>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-md-4 register coach">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
		        		<a href="#" class="button">Next</a>
		        	</div>
		        </div>
	          	
	          </div>
	          <div class="tab-pane fade" id="nav-step2" role="tabpanel" aria-labelledby="nav-step2-tab">
		          	<div class="row">
			        	<div class="col-md-8 left">
			        		<input type="hidden" id="verified" name="verified">
			        		<div class="row">
			        			<div class="col-12">
			        				<h3>Verify Account with Phone Number</h3>
			        				<p>Press “Send Me Code” button to receive a 4 digit code message on your phone.</p>
			        				<p>Just type the code in field below to verify.</p>
			        				<a href="#" class="btn" id="send_code">Send me code</a>

			        				<p>Type 4 digit code here.</p>
			        			</div>
			        		</div>
	        				<div class="row">
	        					<div class="col-md-4">
	        						<div class="form-group mb0">
	        						    <label for="code">Code</label>
	        						    <input type="text" class="form-control" id="code" name="code" required="">
	        						</div>
	        					</div>
	        				</div>
	        				<div class="row">
	        					<div class="col-12">
			        				<h3>Verify account with Email</h3>
			        				<p>Press “Send Me Email” button to receive a verification email, just click on link in your mail to verify.</p>
			        				<a href="#" class="btn" id="send_email">Send me email</a>
			        			</div>
			        		</div>
			        	</div>
			        	<div class="col-md-4 register coach">
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			        		<a href="#" class="button">Next</a>
			        	</div>
			        </div>
	          </div>
	          <div class="tab-pane fade" id="nav-step3" role="tabpanel" aria-labelledby="nav-step3-tab">
	          	<div class="row">
		        	<div class="col-md-8 left">
		        		<div class="row">
		        			<div class="col-12">
		        				<h3>Bank Details</h3>
		        				<p>You will get paid after completing a session</p>
		        			</div>
		        		</div>
        				<div class="row">
        					<div class="col-md-6">
        						<div class="form-group">
        						    <label for="account_name">Account Holder Name</label>
        						    <input type="text" class="form-control" id="account_name" name="account_name" required="">
        						</div>
        					</div>
        					<div class="col-md-6">
        						<div class="form-group">
        						    <label for="account_number">Account Number</label>
        						    <input type="text" class="form-control" id="account_number" name="account_number" required="">
        						</div>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-md-6">
        						<div class="form-group">
        						    <label for="sort_code">Sort / Code</label>
        						    <input type="text" class="form-control" id="sort_code" name="sort_code" required="">
        						</div>
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-12">	
		        				<p class="mb0">First payment will only be taken after your first session takes place.</p>
		        				<p>We will then take a monthly payment as part of your subscription with us.</p>
		        				<h3 class="mb0">Choose Payment Method</h3>
		        			</div>
		        		</div>
			        		<div class="row">
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="">&nbsp;</label>
			        				    <div class="dropdown">
			    		        	    	  <button class="dropdown-toggle custom-select" type="button" id="cardType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    		        	    	   Mastercard/Visa
			    		        	    	  </button>
			    		        	    	  <div class="dropdown-menu" aria-labelledby="cardType">
			    		        	    	  	<div class="dropdown-item">
			    	        	    	  			<input class="radio" id="mastercard" type="radio" name="card_type_inner" required="">
			    	        	    	  			<label for="mastercard">Mastercard</label>
			    		        	    	  	</div>
			    		        	    	  	<div class="dropdown-divider"></div>
			    		        	    	  	<div class="dropdown-item">
			    	        	    	  			<input class="radio" id="visa" type="radio" name="card_type_inner" required="">
			    	        	    	  			<label for="visa">Visa</label>
			    		        	    	  	</div>
			    		        	    	  </div>
			    		        	    	  <input type="hidden" id="card_type" name="card_type">
		    		        	    	</div>
			        				</div>
			        			</div>
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="card_number">Card Number</label>
			        				    <input type="text" class="form-control input-credit-card" id="card_number" name="card_number" required="">
			        				</div>
			        			</div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="name_on_card">Name On Card</label>
			        				    <input type="text" class="form-control" id="name_on_card" name="name_on_card" required="">
			        				</div>
			        			</div>
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="billing_address">Billing Address</label>
			        				    <input type="text" class="form-control" id="billing_address" name="billing_address" required="">
			        				</div>
			        			</div>
			        		</div>
			        		<div class="row">
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="expire_date">Expiry Date</label>
			        				    <input type="text" class="form-control input-date-expire" id="expire_date" placeholder="DD/MM/YYYY" name="expire_date" required="">
			        				</div>
			        			</div>
			        			<div class="col-md-6">
			        				<div class="form-group">
			        				    <label for="cvc">CVC</label>
			        				    <input type="text" class="form-control" id="cvc" placeholder="CVC" name="cvc" required="">
			        				</div>
			        			</div>
			        		</div>
			        	</div>
			        	<div class="col-md-4 register coach">
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			        		<a href="#" class="button">Next</a>
			        	</div>
			        </div>
	          </div>
	          <div class="tab-pane fade" id="nav-step4" role="tabpanel" aria-labelledby="nav-step4-tab">
	          	<div class="row">
		        	<div class="col-md-8 left">
		        		<div class="row">
		        			<div class="col-md-12">
		        				<h3>I'm A Coach In</h3>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group mb0">
		        				    <div class="dropdown">
					        	    	  <button class="dropdown-toggle custom-select" type="button" id="coachType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        	    	   Choose one
					        	    	  </button>
					        	    	  <div class="dropdown-menu" aria-labelledby="coachType">
					        	    	  	<div class="dropdown-item">
				        	    	  			<input class="radio" id="item1" type="radio" name="item1">
				        	    	  			<label for="item1">Item 1</label>
					        	    	  	</div>
					        	    	  	<div class="dropdown-divider"></div>
					        	    	  	<div class="dropdown-item">
				        	    	  			<input class="radio" id="item2" type="radio" name="item2">
				        	    	  			<label for="item2">Item 2</label>
					        	    	  	</div>
					        	    	  </div>
					        	    	  <input type="hidden" id="coach_in_list" name="coach_in_list">
				        	    	</div>
		        				</div>
		        			</div>
		        			<div class="col-md-6">
		        				<div class="form-group mb0">
		        				    <input type="text" class="form-control" id="coach_in" name="coach_in" placeholder="Write here">
		        				</div>
		        			</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-12">
		        				<h3>Upload Your Photo</h3>
		        			</div>
		        		</div>
		        		<div class="row upload-block">
		        			<div class="left">
		        				<div class="file-upload">
		        					<div class="image-upload-wrap">
		        					  <input class="file-upload-input" name="photo" id="photo" type="file" onchange="readURL(this);" accept="image/*">
		        					</div>
		        				  	<button class="file-upload-btn btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload photo</button>
		        				</div>
		        			</div>
		        			<div class="right">
		        				<span class="text">* If you upload a photo you’re more likely to get noticed</span>
		        				<span class="text">JPEG, PNG format only</span>
		        				<span class="text">Minimum image resolution is 2MP</span>
		        				<span class="text">Maximum image resolution is 100MP</span>
		        				<span class="text">Maximum file size is 15MB</span>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-md-4 register coach">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
		        		<button class="button" id="coach_reg">Next</button>
		        	</div>
		        </div>
	          </div>
	        </div>
        </form>
      </div>
    </div>
  </div>
</div>
	<div id="primary" class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 col-lg-4 order-2 order-lg-1">
					<div class="sidebar">
						
<aside id="secondary" class="widget-area">
	<section id="custom_html-2" class="widget_text widget widget_custom_html"><h2 class="widget-title">We Will Help You Choose Best Coach!</h2><div class="textwidget custom-html-widget"><p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nec interdum tortor. Vivamus et nisi vel arcu aliquam sagittis efficitur in arcu. In bibendum, nunc a condimentum suscipit, magna justo suscipit arcu, in ultrices nisl lectus sit amet ipsum.
</p>
<a href="tel:00447479546055" class="phone">++44 7479 546055</a>
<a href="mailto:support@teamalfy.com.com" class="email">support@sportme.com</a></div></section></aside><!-- #secondary -->
					</div>
				</div>
				<div class="col-md-12 col-lg-8 order-1 order-lg-2">
					
					<div role="form" class="wpcf7" id="wpcf7-f4-p69-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/contact-us/#wpcf7-f4-p69-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="4">
<input type="hidden" name="_wpcf7_version" value="5.1.1">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p69-o1">
<input type="hidden" name="_wpcf7_container_post" value="69">
<input type="hidden" name="g-recaptcha-response" value="">
</div>
<div class="row">
<div class="col-12">
		<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Name"></span>
	</div>
</div>
<div class="row">
<div class="col-6">
		<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span>
	</div>
<div class="col-6">
		<span class="wpcf7-form-control-wrap tel-318"><input type="tel" name="tel-318" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" placeholder="Phone"></span>
	</div>
</div>
<div class="row">
<div class="col-12">
		<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Comment.."></textarea></span>
	</div>
</div>
<p><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
				</div>
			</div>
		</div>
	</div><!-- #primary -->


<footer>
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-3">
				<ul class="social">
					<li>
						<a href="https://sportme.teamalfy.com/" class="fb"></a>
					</li>
					<li>
						<a href="https://sportme.teamalfy.com/" class="tw"></a>
					</li>
					<li>
						<a href="https://sportme.teamalfy.com/" class="inst"></a>
					</li>
					<li>
						<a href="https://sportme.teamalfy.com/" class="link"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-6">
				<span class="copy">Copyright (c) All Rights Reserved - <b>SportMe</b></span>
			</div>
			<div class="col-md-3">
				<div class="info">
					<a href="tel:+447479546055" class="phone">+44 7479 546055</a>
					<a href="mailto:support@sportme.com" class="email"> support@sportme.com</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-includes/js/admin-bar.min.js?ver=5.0.3"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/sportmedash.teamalfy.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/slick.min.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/cleave.min.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/cleave-phone.gb.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/phone-type-formatter.gb.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/bootstrap.bundle.min.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/jquery.validate.min.js?ver=20181412"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/navigation.js?ver=20151215"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-content/themes/sportme/js/skip-link-focus-fix.js?ver=20151215"></script>
<script type="text/javascript" src="https://sportmedash.teamalfy.com/wp-includes/js/wp-embed.min.js?ver=5.0.3"></script>
	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://sportmedash.teamalfy.com/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="https://sportmedash.teamalfy.com/wp-admin/about.php">About WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Documentation</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">Support Forums</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://sportmedash.teamalfy.com/wp-admin/">Sportme</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/">Dashboard</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/themes.php">Themes</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/nav-menus.php">Menus</a>		</li>
		<li id="wp-admin-bar-background" class="hide-if-customize"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/themes.php?page=custom-background">Background</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/themes.php?page=custom-header">Header</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/customize.php?url=http%3A%2F%2Fsportmedash.teamalfy.com%2Fcontact-us%2F">Customize</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="https://sportmedash.teamalfy.com/wp-admin/update-core.php" title="3 Plugin Updates"><span class="ab-icon"></span><span class="ab-label">3</span><span class="screen-reader-text">3 Plugin Updates</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="https://sportmedash.teamalfy.com/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://sportmedash.teamalfy.com/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/post-new.php?post_type=page">Page</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/user-new.php">User</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/post.php?post=69&amp;action=edit">Edit Page</a>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://sportmedash.teamalfy.com/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150"><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://sportmedash.teamalfy.com/wp-admin/profile.php">Howdy, <span class="display-name">admin</span><img alt="" src="http://1.gravatar.com/avatar/75d23af433e0cea4c0e45a56dba18b30?s=26&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/75d23af433e0cea4c0e45a56dba18b30?s=52&amp;d=mm&amp;r=g 2x" class="avatar avatar-26 photo" height="26" width="26"></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://sportmedash.teamalfy.com/wp-admin/profile.php"><img alt="" src="http://1.gravatar.com/avatar/75d23af433e0cea4c0e45a56dba18b30?s=64&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/75d23af433e0cea4c0e45a56dba18b30?s=128&amp;d=mm&amp;r=g 2x" class="avatar avatar-64 photo" height="64" width="64"><span class="display-name">admin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="http://sportmedash.teamalfy.com/wp-admin/profile.php">Edit My Profile</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="https://sportmedash.teamalfy.com/wp-login.php?action=logout&amp;_wpnonce=60ea630b8d">Log Out</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="https://sportmedash.teamalfy.com/wp-login.php?action=logout&amp;_wpnonce=60ea630b8d">Log Out</a>
					</div>

		


</body></html>