<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

	<meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Bookings dashboard</title>
	<meta name="description" content="">
	 <meta http-equiv="refresh" content="600">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="{{url('path/to/image.jpg')}}">

	<link rel="shortcut icon" href="{{url('img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{url('img/favicon/apple-touch-icon.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-touch-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-touch-icon-114x114.png')}}">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,700i" rel="stylesheet">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Header CSS (First Sections of Website: paste after release from header.min.css here) -->
	<style></style>

	<!-- Load CSS, CSS Localstorage & WebFonts Main Function -->
	<script>!function(e){"use strict";function t(e,t,n){e.addEventListener?e.addEventListener(t,n,!1):e.attachEvent&&e.attachEvent("on"+t,n)}function n(t,n){return e.localStorage&&localStorage[t+"_content"]&&localStorage[t+"_file"]===n}function a(t,a){if(e.localStorage&&e.XMLHttpRequest)n(t,a)?o(localStorage[t+"_content"]):l(t,a);else{var s=r.createElement("link");s.href=a,s.id=t,s.rel="stylesheet",s.type="text/css",r.getElementsByTagName("head")[0].appendChild(s),r.cookie=t}}function l(e,t){var n=new XMLHttpRequest;n.open("GET",t,!0),n.onreadystatechange=function(){4===n.readyState&&200===n.status&&(o(n.responseText),localStorage[e+"_content"]=n.responseText,localStorage[e+"_file"]=t)},n.send()}function o(e){var t=r.createElement("style");t.setAttribute("type","text/css"),r.getElementsByTagName("head")[0].appendChild(t),t.styleSheet?t.styleSheet.cssText=e:t.innerHTML=e}var r=e.document;e.loadCSS=function(e,t,n){var a,l=r.createElement("link");if(t)a=t;else{var o;o=r.querySelectorAll?r.querySelectorAll("style,link[rel=stylesheet],script"):(r.body||r.getElementsByTagName("head")[0]).childNodes,a=o[o.length-1]}var s=r.styleSheets;l.rel="stylesheet",l.href=e,l.media="only x",a.parentNode.insertBefore(l,t?a:a.nextSibling);var c=function(e){for(var t=l.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){c(e)})};return l.onloadcssdefined=c,c(function(){l.media=n||"all"}),l},e.loadLocalStorageCSS=function(l,o){n(l,o)||r.cookie.indexOf(l)>-1?a(l,o):t(e,"load",function(){a(l,o)})}}(this);</script>

	<!-- Load CSS Start -->
	<script>loadLocalStorageCSS( "webfonts", "{{url('css/fonts.min.css?ver=1.0.0')}}" );</script>
	<script>loadCSS( "{{url('css/header.min.css?ver=1.0.0')}}", false, "all" );</script>
	<script>loadCSS( "{{url('css/main.min.css?ver=1.0.0')}}", false, "all" );</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://www.paypalobjects.com/api/checkout.js"></script>
	<!-- Load CSS End -->

	<!-- Load CSS Compiled without JS -->
	<noscript>
		<link rel="stylesheet" href="{{url('css/fonts.min.cs')}}s">
		<link rel="stylesheet" href="{{url('css/main.min.css')}}">
	</noscript>

</head>

<body>
   


<div class="modal fade" id="proceedModal" tabindex="-1" role="dialog" aria-labelledby="proceedModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bookModalLabel">Choose payment method</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p><b>To book this session, please pay 70£ for 2 hours session with Francis Dos Santos</b></p>
      	<span class="title">Billing methods</span>
      	<ul class="billing">
      		<li>
      			<input class="radio" id="radio-1" type="radio" name="radio" required="" checked>
      			<label for="radio-1">
      				<div class="logo">
      					<img src="./img/mastercard.png" alt="">
      				</div>
      				<span class="text">MasterVard ending in 0415</span>
      			</label>
      		</li>
      		<li>
      			<input class="radio" id="radio-2" type="radio" name="radio" required="" checked>
      			<label for="radio-2">
      				<div class="logo">
      					<img src="./img/paypal.png" alt="">
      				</div>
      				<span class="text">PayPal - someemail@gmail.com</span>
      			</label>
      		</li>
      	</ul>
       	<a href="#" class="btn continue" data-toggle="modal" data-target="#statusModal">Continue</a>
      </div>
    </div>
  </div>
</div>
    
    
    
    <!-- final status -->

    
    
    
    
    
<!-- Here our code  -->
<div class="container-fluid">
	<div class="row">
		<div class="col-12 header-container">
			<header>
				<nav class="navbar navbar-expand-md navbar-light">
				  <a class="navbar-brand col-md-3 " href="{{url('profile/findcoach/home')}}"><img src="{{url('/img/logo.png')}}" alt=""></a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				  	<a href="#" class="back">Back to <b>Profile page</b></a>
				  	<!--
				  	<form class="form-inline my-2 my-lg-0 mr-auto">
				  		<button class="btn my-2 my-sm-0" type="submit"></button>
				  	  	<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				  	</form>
				  	-->
				    <ul class="navbar-nav ml-auto">
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				            Hi, {{ $User_Values->firstname }} {{ $User_Values->lastname }} 
				          <div class="avatar">
				              
				              <?php
									if(empty($User_Values->userimagelink)){
										$url = Storage::url('images/findcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($User_Values->userimagelink);
									}
									?>
				              
				          	<img src="{{asset($url)}}" width="64" height="64" alt="">
				          </div>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="{{url('profile/findcoach/home')}}">Profile</a>
				          <a class="dropdown-item" href="{{url('profile/findcoach/editProfile')}}">Settings</a>
				          <div class="dropdown-divider"></div>
				          <a class="dropdown-item" href="{{url('findcoach/logout')}}">Logout</a>
				        </div>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link settings" href="{{url('profile/findcoach/editProfile')}}"></a>
				      </li>
				    </ul>
				    
				  </div>
				</nav>
			</header>
	</div>

	<div class="sidebar col-md-3 order-2 order-md-1">
		<ul class="menu">
			<li><a href="{{url('profile/findcoach/home')}}" class="type1">Dashboard</a></li>
			<li><a href="{{url('profile/findcoach/home')}}" class="type2 ">Profile</a></li>
			<li><a href="{{url('profile/findcoach/bookingcoachlist')}}" class="type3">Bookings</a></li>
			<li><a href="{{url('profile/findcoach/bookingcoachSessioncomplete')}}" class="type4">Complete Session</a></li>
			<li><a href="{{url('profile/findcoach/coaches')}}" class="type5 active">Coaches</a></li>
			<li><a href="{{url('profile/findcoach/payment')}}" class="type5">Payments</a></li>
			<li><a href="{{url('profile/findcoach/editProfile')}}" class="type7">Settings</a></li>
			@if(session()->has('FindCoachSuperUserInfo_session'))
			    <li><a href="{{url('profile/findcoach/admin')}}" class="type7">Admin Menu</a></li>
			@endif
			<li>
				<a href="{{url('findcoach/logout')}}" class="type8">Logout</a>
				<div class="copyright">All Right Reserved © SportMe.com</div>
			</li>
		</ul>
		
	</div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sorry Some Problem Is Occurred</h5>
                    <a href="{{url('profile/findcoach/coaches')}}">
                    <button type="button" class="close" >
                        
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </a>
                  </div>
                  <div class="modal-body" >
                    <div id="exampleModal_body"></div>
                  </div>
                  <div class="modal-footer">
                      <!--
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                    -->
                  </div>
                </div>
              </div>
            </div>


<div class="content col-md-9 offset-md-3 order-1 order-md-2">
<form action="{{url('profile/findcoach/coachesFind')}}" method="post">	
		<div class="row filter">
			<div class="col-md-8">
				<div class="row">
					<!--
					<div class="col">
		    	    	<div class="form-group">
		        	    	<label for="">Name</label>
		        	    	<input type="text" name="CoachName" id="CoachName" placeholder = "Coach Name">	
		        	    </div>
					</div>
					-->
					<div class="col">
		    	    	<div class="form-group">
		        	    	<label for="">Activity type</label>
		        	    	<input type="text" name="ActivityTypeCoach" id="ActivityTypeCoach" placeholder = "Activity type">
		        	    </div>
					</div>
					<div class="col">
		    	    	<div class="form-group">
		        	    	<label for="">Hourly rate</label>
		        	    	<select class="custom-select" name="PerHourRate">
		        	    	  @for($i = 0;$i<=100;$i++)
		        	    	  <option value="{{$i}}">{{$i}}£/hr</option>
		        	    	  @endfor
		        	    	  
		        	    	  
		        	    	</select>
		        	    </div>
					</div>
				</div>
			</div>
			<div class="col-md-4 text-right">
			    <input type="hidden" value = "" name= "userId">
			    {{@csrf_field()}}
    	    	<button class="btn find-coach">Find coach</button>
			</div>
		</div>
	</form>	
		
		<div class="row">
			<div class="col-12">
				<div class="block">
				    
					<!-- list of bookings -->
					
					@foreach($All_Coachs as $coachs)
					<?php 
                 
                    $d=strtotime("now");
                    //echo date("d/m/Y",$d);
                    $value = \App\CoachBooking::where('im_coach_users_id',$coachs->id)->where('BookingDate',date("d/m/Y",$d))->get();
                    
                    //dd($value);
                    
                    
                  ?>
					
					
					 <div class="modal fade" id="bookModal_{{$coachs->id}}" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="bookModalLabel">Select available day and location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  
                  
                  <form action="javascript:void(0)" id="Form_BookingPayment_{{$coachs->id}}" method="post">
                      <div class="form-group">
                      <label for="sel1">Select Date:</label>
                      <select class="form-control" id="DateOfBookingCoach_{{$coachs->id}}" name ="DateOfBookingCoach">
                          @for($i = 1;$i < 30;$i++)
                          
                          {{$d=strtotime("+".$i." day")}}
                          <option value="{{date("d/m/Y",$d)}}">{{date("d/m/Y",$d)}}</option>
                          @endfor
                        
                       
                      </select>
                    </div>
            
             <div class="form-group">
              <label for="sel1">Session Start Time:</label>
              <select class="form-control" id="StartTimeOfBookingCoach_{{$coachs->id}}" name ="StartTimeOfBookingCoach">
                    
                    <option value="00:00:00">12:00 am</option>
                    <option value="01:00:00">1:00 am</option>
                    <option value="02:00:00">2:00 am</option>
                    <option value="03:00:00">3:00 am</option>
                    <option value="04:00:00">4:00 am</option>
                    <option value="05:00:00">5:00 am</option>
                    <option value="06:00:00">6:00 am</option>
                    <option value="07:00:00">7:00 am</option>
                    <option value="08:00:00">8:00 am</option>
                    <option value="09:00:00">9:00 am</option>
                    <option value="10:00:00">10:00 am</option>
                    <option value="11:00:00">11:00 am</option>
                    <option value="12:00:00">12:00 pm</option>
                    <option value="13:00:00">1:00 pm</option>
                    <option value="14:00:00">2:00 pm</option>
                    <option value="15:00:00">3:00 pm</option>
                    <option value="16:00:00">4:00 pm</option>
                    <option value="17:00:00">5:00 pm</option>
                    <option value="18:00:00">6:00 pm</option>
                    <option value="19:00:00">7:00 pm</option>
                    <option value="20:00:00">8:00 pm</option>
                    <option value="21:00:00">9:00 pm</option>
                    <option value="22:00:00">10:00 pm</option>
                    <option value="23:00:00">11:00 pm</option>

                
               
              </select>
            </div>
            
            
             <div class="form-group">
              <label for="sel1">Session End Time:</label>
              <select class="form-control" id="EndTimeOfBookingCoach_{{$coachs->id}}" name ="EndTimeOfBookingCoach">
                 
                                    
                    <option value="00:00:00">12:00 am</option>
                    <option value="01:00:00">1:00 am</option>
                    <option value="02:00:00">2:00 am</option>
                    <option value="03:00:00">3:00 am</option>
                    <option value="04:00:00">4:00 am</option>
                    <option value="05:00:00">5:00 am</option>
                    <option value="06:00:00">6:00 am</option>
                    <option value="07:00:00">7:00 am</option>
                    <option value="08:00:00">8:00 am</option>
                    <option value="09:00:00">9:00 am</option>
                    <option value="10:00:00">10:00 am</option>
                    <option value="11:00:00">11:00 am</option>
                    <option value="12:00:00">12:00 pm</option>
                    <option value="13:00:00">1:00 pm</option>
                    <option value="14:00:00">2:00 pm</option>
                    <option value="15:00:00">3:00 pm</option>
                    <option value="16:00:00">4:00 pm</option>
                    <option value="17:00:00">5:00 pm</option>
                    <option value="18:00:00">6:00 pm</option>
                    <option value="19:00:00">7:00 pm</option>
                    <option value="20:00:00">8:00 pm</option>
                    <option value="21:00:00">9:00 pm</option>
                    <option value="22:00:00">10:00 pm</option>
                    <option value="23:00:00">11:00 pm</option>

               
              </select>
            </div>
            
            <span class="title">Choose Location</span>
            <span class="text-i">Choose location where you want to meet with this coach</span>
           	<input type="text" class="form-control" placeholder="Meeting Location" name="BookingMeetingLocation" value="{{ $User_Values->location }}" required>
       	    <span class="text-i" id ="TotalTime_intext_{{$coachs->id}}"></span>
       	    <span class="text-i" id="TotalCoachCost_intext_{{$coachs->id}}"></span>
       	    <input type="hidden" name="BookingCoachId" value="{{$coachs->id}}">
       	    <input type="hidden" name="BookingUserId" value="{{$User_Values->id}}">
       	    <input type="hidden" name="UserBookingPrefer" value="{{$User_Values->PaymentPrafer}}">
       	    <input type="hidden" name="CoachParHourCost" value="{{$coachs->parhourCost}}">
       	    @if($User_Values->PaymentPrafer == 0)
       	    <input type="hidden" name="CCName" value="{{$User_Values->CreditCard_CCUserName}}">
       	    <input type="hidden" name="CCNumber" value="{{$User_Values->CreditCard_CCNumber}}">
       	    <input type="hidden" name="CCAddress" value="{{$User_Values->CreditCard_CCBillingAdderss}}">
       	    <input type="hidden" name="CCExpDate" value="{{$User_Values->CreditCard_CCExDate}}">
       	    <input type="hidden" name="Ccv" value="{{$User_Values->CreditCard_CCccv}}">
       	    @endif
       	    {{csrf_field()}}
       	   
       	    
       	    @if($User_Values->PaymentPrafer == 1 || $User_Values->PaymentPrafer == 0)
       	        <button class="btn btn-primary" id="CoachbookingButtonSubmit_{{$coachs->id}}">Proceed to payment</button>
       	         
       	    
       	    @endif
       	   
              
          </form>
          @if($User_Values->PaymentPrafer == 1 )
          <h3> Please complete payment within 30 minutes , because session will be expired. </h3>
       	  @endif
          
      	<!--
      	<div id="datepicker"></div>
      	proceed " data-toggle="modal" data-target="#proceedModal" disabled
      	-->
        
      </div>
    </div>
  </div>
</div>


		<!-- State 2 start-->
		
		
					<?php
					
					
					                if(empty($User_Values->userimagelink)){
										$url = Storage::url('images/findcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($User_Values->userimagelink);
									}
									
								
									?>
				<div class="modal fade" id="statusModal_{{$coachs->id}}" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="statusModalLabel">Payment status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      	<!--
                      	<div class="status-image">
                      	    <div class="avatar">
                      		    <img src="{{asset($url)}}" alt="">
                      		</div>
                      	</div>
                      	-->
                      	<span class="title text-center">Your payment was accepted</span>
                      	<!--
                      	<p class="text-center">You start a session with <b>Francis Dos Santos</b> on <b>01/25/2019 </b><br>
                			from <b>9:00 AM</b> to <b>11:00 AM</b></p>
                        -->
                
                <?php 	if(empty($coachs->userimagelink)){
										$url = Storage::url('images/imcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($coachs->userimagelink);
									}
									?>
                		<a href="#" class="profile-link">
                			<div class="avatar">
                				<img src="{{asset($url)}}" alt="">
                			</div>
                		    {{$coachs->firstname}} {{$coachs->lastname}}
                		</a>
                       	<a href="{{url('profile/findcoach/home')}}" class="btn back"  >Back to dashboard</a>
                      </div>
                    </div>
                  </div>
                </div>	
					
					
					
		<!-- State 2 end-->			
					
					
					
					
					
					
					<div class="coach-item">
						<div class="left">
							<div class="avatar">
								
				              <?php
									if(empty($coachs->userimagelink)){
										$url = Storage::url('images/imcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($coachs->userimagelink);
									}
									?>
				              
				          	<img src="{{asset($url)}}"  alt="">
							</div>
						</div>
						<div class="right">
							<div class="row">
								<div class="col">
									<span class="title-b">{{$coachs->firstname}} {{$coachs->lastname}}</span>
									<span class="title-s">
									    
									    @if($coachs->location != "0")
										{{$coachs->location}}
										@else
										Not Found Location
										@endif
									    
									    
									</span>
								</div>
								<!--
								<div class="col">
									<span class="title-b">Rating</span>
									<ul class="rate_stars">
										<li class="star fill"></li>
										<li class="star fill"></li>
										<li class="star fill"></li>
										<li class="star fill"></li>
										<li class="star half"></li>
									</ul>
									<span class="rate-text">4.67 of 5</span>
								</div>
								
								-->
								<div class="col">
									<span class="title-b">Activity Types</span>
										@if ( $coachs->CoachInterest === '0')
                                                <span class="cat">Not Found Any interested  .</span>
                                            @else ($coachs->CoachInterest != '0')
                                                 <span class="cat"> {{$coachs->CoachInterest}} </span>
                                           
                                            @endif
								</div>
								<div class="col">
									<div class="centered-col">
									    <!--
										<div class="left">
											<span class="title-b">Distance</span>
											<span class="title-s">100 m</span>	
										</div>
										-->
										<div class="right">
										{{$coachs->parhourCost}}£/hr
										</div>
									</div>
								</div>
								<div class="col">
									<a href="#" class="btn" data-toggle="modal" data-target="#bookModal_{{$coachs->id}}">Book</a>
								</div>
							</div>
							<p>{{$coachs->aboutme}}</p>
							</div>
					</div>
					@if($User_Values->PaymentPrafer == 0 || $User_Values->PaymentPrafer == 1)
						<script>
                        	 $(document).ready(function(){
                        	     var hourDiff = 0;
                        	     var totalCost = 0;
                                  $("#Form_BookingPayment_{{$coachs->id}}").change(function(){
                                      var data = jQuery("#Form_BookingPayment_{{$coachs->id}}").serialize();
                                      
                                      
                                      
                                      var valuestart = $("#StartTimeOfBookingCoach_{{$coachs->id}}").val();
                                      var valuestop = $("#EndTimeOfBookingCoach_{{$coachs->id}}").val();
                                      var date = $("DateOfBookingCoach_{{$coachs->id}}").val();
                                      
                                        
                                        //create date format          
                                        var timeStart = new Date("01/01/2007 " + valuestart).getHours();
                                        var timeEnd = new Date("01/01/2007 " + valuestop).getHours();
                                        
                                        
                                         hourDiff = timeEnd - timeStart;  
                                        
                                         totalCost = hourDiff * {{$coachs->parhourCost}} ;
                                          //$("#TotalTime_intext_{{$coachs->id}}").html("You want to start session on the "+date+" from "+valuestart+"  to "+valuestop+"  ");
                                          //$("#TotalCoachCost_intext_{{$coachs->id}}").html("<b>Total to pay: - "+totalCost+"£</b> (Hourly Rate for this coach is <b>{{$coachs->parhourCost}}£/hr</b>)");
                                     
                                      
                                      $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", true);
                                        $('#CoachbookingButtonSubmit_{{$coachs->id}}').html("<i class=\"fa fa-spinner fa-spin\"></i>");
                                     var data = jQuery("#Form_BookingPayment_{{$coachs->id}}").serialize();
                                     var  postValue = "form_type=FindCoach_User_BookingCoachUser&FormType=BookingCoachUser&"+data;
                                                $.ajax({
                                                           type:'POST',
                                                           url:"{{ url('profile/findcoach/bookingcoachAvabiletry') }}",
                                                           beforeSend: function (xhr) {
                                                                var token = $('meta[name="csrf_token"]').attr('content');
                                                    
                                                                if (token) {
                                                                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                                                }
                                                            },
                                                           data:postValue,
                                                           success:function(data) {
                                                               console.log(data);
                                                               var mes = JSON.parse(data);
                                                               console.log(mes)
                                                               if(mes.status == 'success'){
                                                                   
                                                                  
                                                                   
                                                                   $("#TotalTime_intext_{{$coachs->id}}").html("You want to start session on the "+date+" from "+valuestart+"  to "+valuestop+"  ");
                                                                    $("#TotalCoachCost_intext_{{$coachs->id}}").html("<b>Total to pay: - "+totalCost+"£</b> (Hourly Rate for this coach is <b>{{$coachs->parhourCost}}£/hr</b>)");
                                                                     $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", false);
                                                                    $('#CoachbookingButtonSubmit_{{$coachs->id}}').html("Proceed to payment");
                                                               }
                                                               else if(mes.status == 'error'){
                                                                  $("#TotalTime_intext_{{$coachs->id}}").html("Sorry you cannot book this coach at the following time(s):");
                                                                  $("#TotalCoachCost_intext_{{$coachs->id}}").html(mes.message);
                                      
                                     
                                                               }
                                                               
                                                                
                                                           
                                                           },
                                                           error: function (xhr, ajaxOptions, thrownError) {
                                                               console.log(xhr);
                                                            
                                                               console.log(ajaxOptions);
                                                               console.log(ajaxOptions);
                                                                var mes = JSON.parse(data);
                                                                console.log(mes)
                                                                
                                                                
                                                          }
                                                        });
                                     
                                     
                                     
                                     
                                  });
                                  
                                  
                                  $("#Form_BookingPayment_{{$coachs->id}}").submit(function(){
                                      
                                       $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", true);
                                        $('#CoachbookingButtonSubmit_{{$coachs->id}}').html("<i class=\"fa fa-spinner fa-spin\"></i>");
                                      var data = jQuery("#Form_BookingPayment_{{$coachs->id}}").serialize();
                                      
                                      if(hourDiff >= 1 && totalCost >= 1){
                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            });
                                           
                                           
                                            var  postValue = "form_type=FindCoach_User_BookingCoachUser&FormType=BookingCoachUser&"+data;
                                                $.ajax({
                                                           type:'POST',
                                                           url:"{{ url('profile/findcoach/bookingcoachtry') }}",
                                                           beforeSend: function (xhr) {
                                                                var token = $('meta[name="csrf_token"]').attr('content');
                                                    
                                                                if (token) {
                                                                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                                                }
                                                            },
                                                           data:postValue,
                                                           success:function(data) {
                                                               console.log(data);
                                                               var mes = JSON.parse(data);
                                                               console.log(mes)
                                                               if(mes.status == 'success'){
                                                                    @if($User_Values->PaymentPrafer == 0)
                                                                    $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", false);
                                                                    $('#CoachbookingButtonSubmit_{{$coachs->id}}').html("Proceed to payment");
                                                                    $('#bookModal_{{$coachs->id}}').modal('hide');
                                                                    
                                                                    $('#statusModal_{{$coachs->id}}').modal('show');
                                                                    
                                                                    
                                                                   @elseif($User_Values->PaymentPrafer == 1)
                                                                   
                                                                    window.location.href = mes.message;
                                                                   @endif
                                                                    
                                                               }
                                                               else if(mes.status == 'error'){
                                                                   $('#bookModal_{{$coachs->id}}').modal('hide');
                                                                   $('#exampleModal_body').html("<h1>"+mes.message+"</h1>");
                                                                    $('#exampleModal').modal('show');
                                                               }
                                                               
                                                                
                                                           
                                                           },
                                                           error: function (xhr, ajaxOptions, thrownError) {
                                                               console.log(xhr);
                                                            
                                                               console.log(ajaxOptions);
                                                               console.log(ajaxOptions);
                                                                var mes = JSON.parse(data);
                                                                console.log(mes)
                                                                $('#bookModal_{{$coachs->id}}').modal('hide')
                                                                $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", false);
                                                                $('#exampleModal').modal('show');
                                                                
                                                          }
                                                        });
                                                                               
                                          
                                           
                                      }
                                      else{
                                          $('#CoachbookingButtonSubmit_{{$coachs->id}}').attr("disabled", false);
                                         $('#CoachbookingButtonSubmit_{{$coachs->id}}').html("Proceed to payment");
                                          alert("Please Give The Correct Time");
                                      }
                                     
                                  });
                            });   
                    	  
                    	</script>
                    	
                    	@elseif($User_Values->PaymentPrafer == 1)
                    	<script>
                    	/*
                    	 $(document).ready(function(){
                    	     
                    	     $("#paypal-button_{{$coachs->id}}").hide();
                    	    
                    	         var hourDiff = 0;
                        	     var totalCost = 0;
                        	     var data ="";
                                  $("#Form_BookingPayment_{{$coachs->id}}").change(function(){
                                       data = jQuery("#Form_BookingPayment_{{$coachs->id}}").serialize();
                                      
                                      
                                      
                                      var valuestart = $("#StartTimeOfBookingCoach_{{$coachs->id}}").val();
                                      var valuestop = $("#EndTimeOfBookingCoach_{{$coachs->id}}").val();
                                      var date = $("DateOfBookingCoach_{{$coachs->id}}").val();
                                      
                                      
                                        
                                        //create date format          
                                        var timeStart = new Date("01/01/2007 " + valuestart).getHours();
                                        var timeEnd = new Date("01/01/2007 " + valuestop).getHours();
                                        
                                        
                                         hourDiff = timeEnd - timeStart;  
                                        
                                         totalCost = hourDiff * {{$coachs->parhourCost}} ;
                                         
                                         if(hourDiff>=1){
                                              $("#paypal-button_{{$coachs->id}}").show();
                                         }
                                         else{
                                             $("#paypal-button_{{$coachs->id}}").hide();
                                              alert("Please Give The Correct Time");
                                         }
                                         
                    	         
                    	     });
                    	     
                    	     
                    	     
                    	       paypal.Button.render({
                                // Configure environment
                                env: '{{env("PAYPAL_ENV")}}',
                                client: {
                                  sandbox: '{{env("PAYPAL_CLIENT")}}',
                                  production: '{{env("PAYPAL_ENV")}}'
                                },
                                // Customize button (optional)
                                locale: 'en_US',
                                style: {
                                  size: 'responsive',
                                  color: 'gold',
                                  shape: 'pill',
                                  label: 'checkout',
                                  tagline: 'true'
                                },
                            
                                // Enable Pay Now checkout flow (optional)
                                commit: true,
                            
                                // Set up a payment
                                payment: function(data, actions) {
                                  return actions.payment.create({
                                      redirect_urls : {
                                          return_url:'{{url("profile/findcoach/PaymentPaypal")}}'
                                      },
                                    transactions: [{
                                      amount: {
                                        total: totalCost,
                                        currency: 'GBP'
                                      },
                                       experience: {
                                            input_fields: {
                                                no_shipping: 1
                                            }
                                        }
                                    }]
                                  });
                                },
                                // Execute the payment
                                onAuthorize: function(data, actions) {
                                    console.log(data);
                                    console.log(actions);
                                    
                                    console.log(data.returnUrl);
                                    //return actions.redirect();
                                   
                                  return actions.payment.execute().then(function() {
                                    // Show a confirmation message to the buyer
                                    window.alert('Thank you for your purchase!');
                                  });
                                  
                                }
                              }, '#paypal-button_{{$coachs->id}}');
                    	     
                    	 });
                    	 
                    	     
                    	     
                    	
                            
                            */
                            
                            
                        </script>
                    	
                    	
					@endif

                    @endforeach

				
					<!-- list of bookings end -->
					
				</div>
			</div>
		</div>
	</div>




	<div class="hidden"></div>
	

	
        
            	


	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<!-- Load Scripts Start -->
	<script>var scr = {"scripts":[
		{"src" : "{{url('js/libs.js')}}", "async" : false},
		{"src" : "{{url('js/jquery-ui.min.js')}}", "async" : false},
		{"src" : "{{url('js/common.js')}}", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>
	<!-- Load Scripts End -->

</body>
</html>
