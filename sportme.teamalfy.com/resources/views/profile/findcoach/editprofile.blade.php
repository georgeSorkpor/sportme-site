
@extends('layouts.profilemasterlay')
@section('content')



<div class="row">
		<div class="col-12 header-container">
           
           <header>
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand col-md-3 " href="#"><img src="{{ url('img/logo.png') }}" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
            
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <a href="{{ url('profile/findcoach/home') }}" class="back">Back to <b>Home page Of profile</b></a>
                   
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                        
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Hi, {{ $User_Values->firstname  }} 
                            
                            <div class="avatar">
                            
								<div class="col-md-3">
									<?php
									if(empty($User_Values->userimagelink)){
										$url = Storage::url('images/findcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($User_Values->userimagelink);
									}
									?>
									<div class="image-block">
										
								    <img src="{{asset($url)}}" class="rounded mx-auto d-block" >
									</div>
								</div>
								
                            </div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                             <a class="dropdown-item" href="{{ url('profile/findcoach/home') }}">Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('findcoach/logout') }}">Log out</a>
                        </div>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link settings" href="{{ url('profile/findcoach/editProfile') }}"></a>
                        </li>
                    </ul>
                    
                    </div>
                </nav>
            </header>
           
           
           
	    </div>
			
	</div>
	
	
	
	
	
	<div class="sidebar col-md-3 order-2 order-md-1">
    <ul class="menu">
        <li><a href="{{url('profile/findcoach/home')}}" class="type1">Dashboard</a></li>
        <li><a href="{{url('profile/findcoach/home')}}" class="type2 ">Profile</a></li>
        <li><a href="{{url('profile/findcoach/bookingcoachlist')}}" class="type3 ">Bookings</a></li>
       
        
        <li><a href="{{url('profile/findcoach/bookingcoachSessioncomplete')}}" class="type4 ">Completed Sessions</a></li>
        <li><a href="{{url('profile/findcoach/coaches')}}" class="type9 ">Coaches</a></li>
       
        <li><a href="{{url('profile/findcoach/payment')}}" class="type5">Payments</a></li>
        <li><a href="{{ url('profile/findcoach/editProfile') }}" class="type7">Settings</a></li>
        <li>
            <a href="{{ url('findcoach/logout') }}" class="type8">Logout</a>
            <div class="copyright">All Right Reserved © SportMe.com</div>
        </li>
    </ul>
    
</div>












	<div class="content col-md-9 offset-md-3 order-1 order-md-2">
		<form action="{{url('profile/findcoach/editProfiletry')}}" method="POST" enctype="multipart/form-data">
		    {{csrf_field()}}
          <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="Fname" value="{{$User_Values->firstname}}" name="Fname" placeholder="Enter First Name">
            
          </div>
          
          
          
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="Lname" name="Lname" value="{{$User_Values->lastname}}" >
          </div>
          
           <div class="form-group">
            <label for="exampleInputEmail1">Phone Number</label>
            <input type="text" class="form-control" id="PhoneNumber" name="PhoneNumber" value="{{$User_Values->phonenumber}}">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">About Me</label>
             <textarea class="form-control" id="AboutMe" name="AboutMe" rows="3">{{$User_Values->aboutme}}</textarea>
          </div>
          
          
          <div class="form-group">
            <label for="exampleInputEmail1">Workout Goal :</label>
            <textarea class="form-control" id="WorkoutGoal" name="WorkoutGoal" rows="3">{{$User_Values->workoutGoal}}</textarea>
          </div>
          
	        <div class="form-group">
	            
            <label for="exampleFormControlFile1">Profile Picture Upload</label>
            <input type="file" class="form-control-file" id="ImageFile" name="ImageFile">
            
          </div>
          
          <input type="hidden"  id="UserId" name="UserId" value="{{$User_Values->id}}">
          <input type="hidden"  id="Useremail" name="Useremail" value="{{$User_Values->email}}">
          <input type="hidden"  id="ImageLink" name="ImageLink" value="{{$User_Values->userimagelink}}">
           <button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
@endsection


<!--
<pre>
   {{print_r($User_Values)}}
</pre>
-->