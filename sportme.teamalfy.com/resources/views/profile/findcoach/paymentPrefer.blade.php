<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

	<meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Payments dashboard</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="{{url('path/to/image.jpg')}}">

	<link rel="shortcut icon" href="{{url('img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{url('img/favicon/apple-touch-icon.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-touch-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-touch-icon-114x114.png')}}">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,700i" rel="stylesheet">
	<!-- Header CSS (First Sections of Website: paste after release from header.min.css here) -->
	<style></style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Load CSS, CSS Localstorage & WebFonts Main Function -->
	<script>!function(e){"use strict";function t(e,t,n){e.addEventListener?e.addEventListener(t,n,!1):e.attachEvent&&e.attachEvent("on"+t,n)}function n(t,n){return e.localStorage&&localStorage[t+"_content"]&&localStorage[t+"_file"]===n}function a(t,a){if(e.localStorage&&e.XMLHttpRequest)n(t,a)?o(localStorage[t+"_content"]):l(t,a);else{var s=r.createElement("link");s.href=a,s.id=t,s.rel="stylesheet",s.type="text/css",r.getElementsByTagName("head")[0].appendChild(s),r.cookie=t}}function l(e,t){var n=new XMLHttpRequest;n.open("GET",t,!0),n.onreadystatechange=function(){4===n.readyState&&200===n.status&&(o(n.responseText),localStorage[e+"_content"]=n.responseText,localStorage[e+"_file"]=t)},n.send()}function o(e){var t=r.createElement("style");t.setAttribute("type","text/css"),r.getElementsByTagName("head")[0].appendChild(t),t.styleSheet?t.styleSheet.cssText=e:t.innerHTML=e}var r=e.document;e.loadCSS=function(e,t,n){var a,l=r.createElement("link");if(t)a=t;else{var o;o=r.querySelectorAll?r.querySelectorAll("style,link[rel=stylesheet],script"):(r.body||r.getElementsByTagName("head")[0]).childNodes,a=o[o.length-1]}var s=r.styleSheets;l.rel="stylesheet",l.href=e,l.media="only x",a.parentNode.insertBefore(l,t?a:a.nextSibling);var c=function(e){for(var t=l.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){c(e)})};return l.onloadcssdefined=c,c(function(){l.media=n||"all"}),l},e.loadLocalStorageCSS=function(l,o){n(l,o)||r.cookie.indexOf(l)>-1?a(l,o):t(e,"load",function(){a(l,o)})}}(this);</script>

	<!-- Load CSS Start -->
	<script>loadLocalStorageCSS( "webfonts", "{{url('css/fonts.min.css?ver=1.0.0')}}" );</script>
	<script>loadCSS( "{{url('css/header.min.css?ver=1.0.0')}}", false, "all" );</script>
	<script>loadCSS( "{{url('css/main.min.css?ver=1.0.0')}}", false, "all" );</script>
	<!-- Load CSS End -->

	<!-- Load CSS Compiled without JS -->
	<noscript>
		<link rel="stylesheet" href="{{url('css/fonts.min.css')}}">
		<link rel="stylesheet" href="{{url('css/main.min.css')}}">
	</noscript>

</head>

<body>
<!-- Here our code  -->
<!-- Modal -->
<div class="modal fade" id="methodsModal" tabindex="-1" role="dialog" aria-labelledby="methodsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="methodsModalLabel">Add a billing method</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input class="radio" id="radio-1" type="radio" name="radio" required="" checked>
        <label for="radio-1">Credit or Debit Card</label>
        <form action="{{ url('profile/findcoach/paymenttry') }}" id= "CreditCardInformation" method ="post">
        	<div class="form-group">
        	    <label for="cardNumber">Card Number 
        	    	<div class="available">
        	    		<img src="{{asset('img/visa.png')}}" alt="">
						<img src="{{asset('img/master.png')}}" alt="">
        	    	</div>
        	    </label>
        	    <input type="text" class="form-control" id="cardNumber" value="{{$User_Values->CreditCard_CCNumber}}" name="CCNumber" placeholder="">
        	</div>
        	<div class="row">
        	    <div class="co-12 col-md">
        	    	<div class="form-group">
	        	    	<label for="">Name On Card</label>
	        	      	<input type="text" class="form-control" value="{{$User_Values->CreditCard_CCUserName}}" name="CCName">
	        	    </div>
        	    </div>
        	    <div class="co-12 col-md">
        	    	<div class="form-group">
	        	    	<label for="">Address :</label>
	        	      	<input type="text" class="form-control" value="{{$User_Values->CreditCard_CCBillingAdderss}}" name="CCAddress">
        	      	</div>
        	    </div>
    	    </div>
        	<div class="row">
        	    <div class="co-12 col-md">
        	    	<div class="form-group">
	        	    	<label for="">Expires On</label>
	        	      	<input type="text" class="form-control" placeholder="MM/YY" value = "{{$User_Values->CreditCard_CCExDate}}" name="CCExpDate">
        	      	</div>
        	    </div>
        	    <div class="co-12 col-md">
        	    	<div class="form-group">
	        	    	<label for="">Security Code</label>
	        	      	<input type="text" class="form-control" name="Ccv" value = "{{$User_Values->CreditCard_CCccv}}">
        	      	</div>
        	    </div>
    	    </div>
    	    {{@csrf_field()}}
    	    <input type="hidden" value="{{$User_Values->id}}" name="UserId">
    	    <button  class="btn" id="CraditCardSubmitbutton">Continue</button>
        </form>
      </div>
      
      <!--
      <div class="modal-footer">
		<div class="block">
			<div class="col-12">
				<input class="radio" id="radio-2" type="radio" name="radio" required="">
				<label for="radio-2">
					<div class="logo">
			      	<img src="./img/paypal.png" alt="">
			      </div>
			  	</label>
			</div>
		</div>
        <div class="form-group col-12">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email">
        </div>
      </div>
      -->
      
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-12 header-container">
			<header>
				<nav class="navbar navbar-expand-md navbar-light">
				  <a class="navbar-brand col-md-3 " href="#"><img src="{{url('/img/logo.png')}}" alt=""></a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				  	<a href="{{url("/")}}" class="back">Back to <b>Profile page</b></a>
				  	<!--
				  	<form class="form-inline my-2 my-lg-0 mr-auto">
				  		<button class="btn my-2 my-sm-0" type="submit"></button>
				  	  	<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				  	</form>
				  	-->
				    <ul class="navbar-nav ml-auto">
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Hi, Marcus 
				          <div class="avatar">
				          	<img src="./img/avatar.png" width="64" height="64" alt="">
				          </div>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="#">Action</a>
				          <a class="dropdown-item" href="#">Another action</a>
				          <div class="dropdown-divider"></div>
				          <a class="dropdown-item" href="#">Something else here</a>
				        </div>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link settings" href="#"></a>
				      </li>
				    </ul>
				    
				  </div>
				</nav>
			</header>
		</div>

	<div class="sidebar col-md-3 order-2 order-md-1">
		<ul class="menu">
			<li><a href="{{url('profile/findcoach/home')}}" class="type1">Dashboard</a></li>
			<li><a href="{{url('profile/findcoach/home')}}" class="type2 ">Profile</a></li>
			<li><a href="{{url('profile/findcoach/bookingcoachlist')}}" class="type3">Bookings</a></li>
			<li><a href="{{url('profile/findcoach/bookingcoachSessioncomplete')}}" class="type4">Complete Session</a></li>
			<li><a href="{{url('profile/findcoach/coaches')}}" class="type5 ">Coaches</a></li>
			<li><a href="{{url('profile/findcoach/payment')}}" class="type5 active">Payments</a></li>
			<li><a href="{{url('profile/findcoach/editProfile')}}" class="type7">Settings</a></li>
			@if(session()->has('FindCoachSuperUserInfo_session'))
			    <li><a href="{{url('profile/findcoach/admin')}}" class="type7">Admin Menu</a></li>
			@endif
			<li>
				<a href="{{url('findcoach/logout')}}" class="type8">Logout</a>
				<div class="copyright">All Right Reserved © SportMe.com</div>
			</li>
		</ul>
		
	</div>

	<div class="content col-md-9 offset-md-3 order-1 order-md-2">
	    
		<div class="row">
		    
			<div class="col-12">
				<div class="block">
				    <center>
				     @if ($errors->any())
                        <div class="alert alert-danger" >
                            <center>
                                 @foreach ($errors->all() as $error)
                                    {{ $error }}
                                 @endforeach
                            </center>
                               
                        </div>
                    @endif
                    </center>
					<h3>Billing and payments</h3>
					<!--
					<a href="#" class="btn add-methods" data-toggle="modal" data-target="#methodsModal">Add methods</a>
					-->
					
					<div class="block-inner">
						<span class="title-inner">Billing methods</span>
						<span class="type">Primary</span>

						<div class="row align-items-center">
							<div class="col-12 col-lg-6">
								<div class="flex-center">
									<div class="logo">
										<img src="./img/mastercard.png" alt="">
									</div>
									<span class="text">Card Details</span>
									<span class="text last">GBP</span>
									<a href="#" class="settings" data-toggle="modal" data-target="#methodsModal"></a>
								</div>
							</div>
							<div class="col-md-6">
								<input class="checkbox" id="checkbox-1" type="radio" name="checkbox" required="" <?php if($User_Values->PaymentPrafer == 0)echo "checked"; ?> >
								<label for="checkbox-1">Preferred</label>
							</div>
						</div>

						<span class="type">Additional</span>
						<div class="row align-items-center">
							<div class="col-12 col-lg-6">
								<div class="flex-center">
									<div class="logo">
										<img src="./img/paypal.png" alt="">
									</div>
									<span class="text">PayPal </span>
									<!--<a href="#" class="settings"></a>-->
								</div>
							</div>
							<div class="col-md-6">
								<input class="checkbox" id="checkbox-2" type="radio" name="checkbox" required=""  <?php if($User_Values->PaymentPrafer == 1)echo "checked"; ?> >
								<label for="checkbox-2">Preferred</label>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

	<div class="hidden"></div>

<script>
    
    $(document).ready(function(){
        
          $('#checkbox-2').change(function () {
                alert('changed_2');
                
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                
                 var  postValue = "form_type=FindCoach_User_PaymentPrefer&FormType=PaymentPrefer&checkbox=1&id={{$User_Values->id}}";
            $.ajax({
                       type:'POST',
                       url:"{{ url('profile/findcoach/paymenttry2') }}",
                       beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');
                
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                       data:postValue,
                       success:function(data) {
                           console.log(data);
                           window.location.href = window.location.href;
                            
                       
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           console.log(xhr);
                           console.log(ajaxOptions);
                           console.log(ajaxOptions);
                           
                           
                            
                      }
                });
                
           });    
           
            $('#checkbox-1').change(function () {
                alert('changed_1');
                
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                
                 var  postValue = "form_type=FindCoach_User_PaymentPrefer&FormType=PaymentPrefer&checkbox=0&id={{$User_Values->id}}";
                $.ajax({
                           type:'POST',
                           url:"{{ url('profile/findcoach/paymenttry2') }}",
                           beforeSend: function (xhr) {
                                var token = $('meta[name="csrf_token"]').attr('content');
                    
                                if (token) {
                                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            },
                           data:postValue,
                           success:function(data) {
                               console.log(data);
                               window.location.href = window.location.href;
                                
                           
                           },
                           error: function (xhr, ajaxOptions, thrownError) {
                               console.log(xhr);
                               console.log(ajaxOptions);
                               console.log(ajaxOptions);

                          }
                    });
                
                
                
                
                
                
                
                
           });
            
        });

</script>


	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<!-- Load Scripts Start -->
	<script>var scr = {"scripts":[
		{"src" : "{{url('js/libs.js')}}", "async" : false},
		{"src" : "{{url('js/common.js')}}", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>
	<!-- Load Scripts End -->

</body>
</html>
