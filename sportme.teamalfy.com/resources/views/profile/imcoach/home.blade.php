<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

	<meta charset="utf-8">

	<title>Profile dashboard</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="path/to/image.jpg">

	<link rel="shortcut icon" href="{{url('img/favicon/favicon.ico')}}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{url('img/favicon/apple-touch-icon.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-touch-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-touch-icon-114x114.png')}}">
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,700,700i" rel="stylesheet">
	<!-- Header CSS (First Sections of Website: paste after release from header.min.css here) -->
	<style></style>

	<!-- Load CSS, CSS Localstorage & WebFonts Main Function -->
	<script>!function(e){"use strict";function t(e,t,n){e.addEventListener?e.addEventListener(t,n,!1):e.attachEvent&&e.attachEvent("on"+t,n)}function n(t,n){return e.localStorage&&localStorage[t+"_content"]&&localStorage[t+"_file"]===n}function a(t,a){if(e.localStorage&&e.XMLHttpRequest)n(t,a)?o(localStorage[t+"_content"]):l(t,a);else{var s=r.createElement("link");s.href=a,s.id=t,s.rel="stylesheet",s.type="text/css",r.getElementsByTagName("head")[0].appendChild(s),r.cookie=t}}function l(e,t){var n=new XMLHttpRequest;n.open("GET",t,!0),n.onreadystatechange=function(){4===n.readyState&&200===n.status&&(o(n.responseText),localStorage[e+"_content"]=n.responseText,localStorage[e+"_file"]=t)},n.send()}function o(e){var t=r.createElement("style");t.setAttribute("type","text/css"),r.getElementsByTagName("head")[0].appendChild(t),t.styleSheet?t.styleSheet.cssText=e:t.innerHTML=e}var r=e.document;e.loadCSS=function(e,t,n){var a,l=r.createElement("link");if(t)a=t;else{var o;o=r.querySelectorAll?r.querySelectorAll("style,link[rel=stylesheet],script"):(r.body||r.getElementsByTagName("head")[0]).childNodes,a=o[o.length-1]}var s=r.styleSheets;l.rel="stylesheet",l.href=e,l.media="only x",a.parentNode.insertBefore(l,t?a:a.nextSibling);var c=function(e){for(var t=l.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){c(e)})};return l.onloadcssdefined=c,c(function(){l.media=n||"all"}),l},e.loadLocalStorageCSS=function(l,o){n(l,o)||r.cookie.indexOf(l)>-1?a(l,o):t(e,"load",function(){a(l,o)})}}(this);</script>

	<!-- Load CSS Start -->
	<script>loadLocalStorageCSS( "webfonts", "{{url('css/fonts.min.css?ver=1.0.0')}}" );</script>
	<script>loadCSS( "{{url('css/header.min.css?ver=1.0.0')}}", false, "all" );</script>
	<script>loadCSS( "{{url('css/main.min.css?ver=1.0.0')}}", false, "all" );</script>
	<!-- Load CSS End -->

	<!-- Load CSS Compiled without JS -->
	<noscript>
		<link rel="stylesheet" href="css/fonts.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</noscript>

</head>

<body>
<!-- Here our code  -->
<div class="container-fluid">
	<div class="row">
		<div class="col-12 header-container">
			<header>
				<nav class="navbar navbar-expand-md navbar-light">
				  <a class="navbar-brand col-md-3 " href="#"><img src="{{url('/img/logo.png')}}" alt=""></a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				  	<a href="{{url('profile/imcoach/home')}}" class="back">Back to <b>Home page</b></a>
				  	<!--
				  	<form class="form-inline my-2 my-lg-0 mr-auto">
				  		<button class="btn my-2 my-sm-0" type="submit"></button>
				  	  	<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				  	</form>
				  	
				  	-->
				    <ul class="navbar-nav ml-auto">
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Hi, {{ $User_Values->firstname." ".$User_Values->lastname }}  
				          <div class="avatar">
				              
				              <?php
									if(empty($User_Values->userimagelink)){
										$url = Storage::url('images/imcoachs/profilepic/default_pic.jpg');
									}
									else{
									    $url = Storage::url($User_Values->userimagelink);
									}
									?>
				              
				          	<img src="{{asset($url)}}" width="64" height="64" alt="">
				          </div>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="{{url('profile/imcoach/home')}}">Profile</a>
				          <a class="dropdown-item" href="{{url('profile/imcoach/editProfile')}}">Setting</a>
				          <div class="dropdown-divider"></div>
				          <a class="dropdown-item" href="{{url('imcoach/logout')}}">Logout</a>
				        </div>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link settings" href="#"></a>
				      </li>
				    </ul>
				    
				  </div>
				</nav>
			</header>
	</div>

	<div class="sidebar col-md-3 order-2 order-md-1">
		<ul class="menu">
			<li><a href="{{url('profile/imcoach/home')}}" class="type1">Dashboard</a></li>
			<li><a href="{{url('profile/imcoach/home')}}" class="type2 active">Profile</a></li>
			<li><a href="{{url('profile/imcoach/booking')}}" class="type3">Bookings</a></li>
			<li><a href="#" class="type4">Availibility</a></li>
			<li><a href="#" class="type5">Payments</a></li>
			<li><a href="#" class="type6">Reviews</a></li>
			<li><a href="{{url('profile/imcoach/editProfile')}}" class="type7">Settings</a></li>
		
			
			<li>
				<a href="{{url('/imcoach/logout')}}" class="type8">Logout</a>
				<div class="copyright">All Right Reserved © SportMe.com</div>
			</li>
		</ul>
		
	</div>

	<div class="content col-md-9 offset-md-3 order-1 order-md-2">
		<div class="row">
			<div class="col-12">
				<div class="block">
					<a href="{{url('profile/imcoach/editProfile')}}" class="edit group"></a>
					<div class="row">
						<div class="col-lg-12 col-xl-8">
							<div class="row align-items-start">
								<div class="col-md-3">
									<div class="image-block">
										<img width="150" src="{{asset($url)}}" alt="">
									</div>
								</div>
								<div class="col-md-9">
									<div class="row">
										<div class="col-md-6 col-xl-5">
											<span class="name">{{ $User_Values->firstname." ".$User_Values->lastname }}  </span>
											@if ( $User_Values->CoachInterest === '0')
                                                <span class="cat">I'm not interested with default value .</span>
                                            @else ($User_Values->CoachInterest != '0')
                                                
                                                <span class="cat"> {{$User_Values->CoachInterest}} </span>
                                           
                                            @endif
                                            
										</div>
										<div class="col-md-6 col-xl-7">
											<span class="price">${{$User_Values->parhourCost}}/hr</span>
										</div>
									</div>
									<div class="row">
										<div class="col-md-5">
											<span class="exp">Years of experience: <b>{{$User_Values->ImCoachexperince}}</b></span>
										</div>
									
										<div class="col-md-7">
										    
											<div class="rate">
												<ul class="rate_stars">
												    @for($i = 1; $i<= (int)$User_Values->rateingCoach;$i++)
												    	<li class="star"></li>
												    @endfor
												
												
												</ul><br>
												<span class="rate-text"> {{$User_Values->rateingCoach}} Rating </span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<div class="buttons">
											    <!--
												 <a href="#" class="btn">Contact</a>
												<a href="#" class="btn">Book</a>
												
												-->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-xl-4">
							<table>
								<tbody>
									<tr>
									    <?php
									    $bool = false;
									    try{
									        $bthday = $User_Values->birthday;
									        $date = explode("/",$bthday);
									        $bool = checkdate($date[1],$date[0],$date[2]);
									    }catch(\Exception $e){
									        
									    }
									    ?>
									    
										<td><b>Age:</b></td>
										@if ( $bool)
										<?php
										$age = (date("md", date("U", mktime(0, 0, 0, $date[1], $date[0], $date[2]))) > date("md") ? ((date("Y") - $date[2]) - 1) : (date("Y") - $date[2]));
										?>
										<td>{{$age}}</td>
										@else
										<td>Please Give Correct Date Of Birth</td>
										@endif
									</tr>
									<tr>
										<td><b>Locations:</b></td>
										
										@if($User_Values->location != "0")
										<td>{{$User_Values->location}}</td>
										@else
										<td>Please Give Correct Location </td>
										@endif
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="title-block">
					<span>Profile</span>
				</div>
				<div class="block">
					<a href="{{url('profile/imcoach/editProfile')}}" class="edit"></a>
					<span class="title">About Me</span>
					<p style="white-space: pre-line">{{$User_Values->aboutme}}</p>
					

				</div>
			</div>
		</div>
	</div>
	</div>
</div>

	<div class="hidden"></div>

	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<!-- Load Scripts Start -->
	<script>var scr = {"scripts":[
		{"src" : "{{url('js/libs.js')}}", "async" : false},
		{"src" : "{{url('js/common.js')}}", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>
	<!-- Load Scripts End -->

</body>
</html>
