<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFindCoachUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('find_coach_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usertype')->default('1');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password')->default('0');
            $table->string('phonenumber')->default('0');
            $table->string('birthday')->default('0');
            $table->string('sport_interest')->default('0');
            $table->string('location')->default('0');
           
            $table->tinyInteger('isverifyemailstatus')->default('0');
            $table->timestamp('email_verified_at')->nullable();
            
            $table->string('CreditCard_CCNumber')->nullable();
            $table->string('CreditCard_CCUserName')->nullable();
            $table->string('CreditCard_CCBillingAdderss')->nullable();
            $table->string('CreditCard_CCExDate')->nullable();
            $table->string('CreditCard_CCccv')->nullable();
             $table->longText('StripeObjectValue')->nullable();
            
            $table->string('Paypal_Email')->nullable();
            $table->longText('PaypalObjectValue')->nullable();
            
            $table->string('PaymentPrafer')->default('0');
            
            $table->string('userimagetype')->nullable();
            $table->string('userimagelink')->nullable();
            $table->longText('aboutme')->nullable();
            $table->longText('workoutGoal')->nullable();
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('find_coach_users');
    }
}
