<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_coach_users_id');
            $table->string('find_coach_users_id');
            $table->string('BookingDate')->nullable();
            $table->string('BookingTime')->nullable();
            $table->timestamp('BookingTimeDate')->nullable();
            $table->string('BookingTotalHours')->default(0);
            $table->string('ParhourCoachAmount')->default(0);
            $table->string('totalAmount')->default(0);
            $table->string('Location')->nullable();
            $table->string('PaymentWayFindCoach')->nullable();
            $table->longtext('PaymentFindCoachAllInfo')->nullable();
            $table->longtext('ActivityType')->nullable();
            $table->string('isReviewMessageSend')->default(0);
            $table->string('isSessionComplete')->default(0);
            $table->timestamp('BookingSessionComplitedTime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_bookings');
    }
}
