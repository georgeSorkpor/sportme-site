<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImCoachUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_coach_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usertype')->default('2');
            $table->string('userstepsCompleted')->default('1');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password')->default('0');
            $table->string('phonenumber')->default('0');
            $table->string('phonenumberCountryCode')->default('0');
            $table->string('phonenumberNationalForm')->default('0');
            $table->string('birthday')->default('0');
            $table->string('location')->default('0');
            
            
            
            $table->string('isMobileVerification')->nullable();
            $table->string('MobileVerificationCode')->nullable();
            $table->string('isEmailVerification')->nullable();
            $table->string('EmailVerificationCode')->nullable();
            
            
            $table->string('BankDetails_AccountHolderName')->nullable();
            $table->string('BankDetails_BankName')->nullable();
            $table->string('BankDetails_AccountNumber')->nullable();
            $table->string('BankDetails_AccountSortCode')->nullable();
            $table->string('BankDetails_Adress')->nullable();
            
            
            $table->string('CreditCard_CCNumber')->nullable();
            $table->string('CreditCard_CCUserName')->nullable();
            $table->string('CreditCard_CCBillingAdderss')->nullable();
            $table->string('CreditCard_CCExDate')->nullable();
            $table->string('CreditCard_CCccv')->nullable();
            $table->longText('StripeObjectValue')->nullable();
            
            $table->string('Paypal_Email')->nullable();
            $table->longText('PapalObjectValue')->nullable();
            
            $table->string('PaymentPrafer')->default('0');
            
            
            $table->string('CoachInterest')->default('0');
           
            $table->string('userimagetype')->nullable();
            $table->string('userimagelink')->nullable();
            $table->longText('aboutme')->nullable();
            $table->string('parhourCost')->default('0');
            $table->string('rateingCoach')->default('0');
            $table->string('ImCoachexperince')->default('0');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('im_coach_users');
    }
}
